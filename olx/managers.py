import json
from time import sleep
import time
import requests
from bs4 import BeautifulSoup
import random
from typing import Dict, List
from config import (
    HEADERS,
    DOMAIN,
    DOPOMOGA,
    TRANSPORTS,
    DETSKIY_MIR,
    NEDVIZHIMOST,
    ZAPCHASTI_DLYA_TRNSPORTA,
    ZHIVOTNYE,
    ELEKTRONIKA,
    DOM_I_SAD,
    RABOTA,
    USLUGI,
    ARENDA_PROKAT,
    MODA_I_STIL,
    HOBBI,
    OTDAM_DAROM,
    OBMEN,
    TODAY,
    ANSWER,
)


class ScrapingOlx:
    @classmethod
    def session(cls) -> requests.sessions.Session:
        return requests.Session()

    _domain = DOMAIN
    _headers = HEADERS
    _today = TODAY
    _answer = ANSWER

    _dopomoga_list_uri = DOPOMOGA
    _transport_list_uri = TRANSPORTS
    _detskiy_mir_list_uri = DETSKIY_MIR
    _nedvizhimost_list_uri = NEDVIZHIMOST
    _zapchasti_dlya_transporta_list_uri = ZAPCHASTI_DLYA_TRNSPORTA
    _zhivotnye_list_uri = ZHIVOTNYE
    _elektronika_list_uri = ELEKTRONIKA
    _dom_i_sad_list_uri = DOM_I_SAD
    _rabota_list_uri = RABOTA
    _uslugi_list_uri = USLUGI
    _arenda_prokat_list_uri = ARENDA_PROKAT
    _moda_i_stil_list_uri = MODA_I_STIL
    _hobbi_list_uri = HOBBI
    _otdam_darom_list_uri = OTDAM_DAROM
    _obmen_list_uri = OBMEN

    @property
    def domain(self) -> str:
        return self._domain

    @domain.setter
    def domain(self, new_domain: str):
        self.domain = new_domain

    @property
    def headers(self) -> Dict:
        return self._headers

    @headers.setter
    def headers(self, new_headers: Dict):
        self.headers = new_headers

    @property
    def uri(self) -> str:
        return self._uri

    @uri.setter
    def uri(self, new_uri: str):
        self._uri = new_uri

    @property
    def today(self) -> str:
        return self._today

    @property
    def file_name(self) -> str:
        return self._file_name

    @file_name.setter
    def file_name(self, new_file_name: str):
        self._file_name = new_file_name

    @property
    def answer(self) -> str:
        return self._answer

    @answer.setter
    def answer(self, new_answer: str):
        self._answer = new_answer

    @property
    def dopomoga_list_uri(self) -> List:
        return self._dopomoga_list_uri

    @property
    def transport_list_uri(self):
        return self._transport_list_uri

    @property
    def detskiy_mir_list_uri(self) -> List:
        return self._detskiy_mir_list_uri

    @property
    def nedvizhimost_list_uri(self) -> List:
        return self._nedvizhimost_list_uri

    @property
    def zapchasti_dlya_transporta_list_uri(self) -> List:
        return self._zapchasti_dlya_transporta_list_uri

    @property
    def zhivotnye_list_uri(self) -> List:
        return self._zhivotnye_list_uri

    @property
    def elektronika_list_uri(self) -> List:
        return self._elektronika_list_uri

    @property
    def dom_i_sad_list_uri(self) -> List:
        return self._dom_i_sad_list_uri

    @property
    def rabota_list_uri(self) -> List:
        return self._rabota_list_uri

    @property
    def uslugi_list_uri(self) -> List:
        return self._uslugi_list_uri

    @property
    def arenda_prokat_list_uri(self) -> List:
        return self._arenda_prokat_list_uri

    @property
    def moda_i_stil_list_uri(self) -> List:
        return self._moda_i_stil_list_uri

    @property
    def hobbi_list_uri(self) -> List:
        return self._hobbi_list_uri

    @property
    def otdam_darom_list_uri(self) -> List:
        return self._otdam_darom_list_uri

    @property
    def obmen_list_uri(self) -> List:
        return self._obmen_list_uri

    def sync_request(
        self,
        method: str,
        uri: str,
        headers=None,
        data=None,
    ) -> Dict:
        session = self.session()
        if method == "GET":
            response = session.get(uri, headers=headers)
        if method == "POST":
            response = session.post(uri, headers=headers, data=data)
        try:
            code = response.status_code
            response.raise_for_status()
            random_time = random.randint(1, 3)
            sleep(random_time)
            content = response.text
            return content
        except requests.exceptions.HTTPError as exc:
            error_response = {"code": code, "detail": str(exc)}
            print(error_response)

    def get_pagination(self, content):
        soup = BeautifulSoup(content, "lxml")
        try:
            payload = (
                soup.find(class_="css-1nvt13t")
                .find(class_="css-4mw0p4")
                .find(class_="css-1vdlgt7")
                .find_all("a")
            )
            links_list = []
            for _ in payload:
                data_testid = _.get("data-testid")
                links_list.append(data_testid)
            pagination = links_list[-1]
            if pagination == "pagination-forward":
                return True
            return False
        except Exception as exc:
            print(str(exc))

    def get_pages(self):
        i = 0
        while True:
            i += 1
            if i == 1:
                uri = self.domain + self.uri
            else:
                uri = self.domain + self.uri + f"?page={i}"
            content = self.sync_request(method="GET", uri=uri, headers=self.headers)
            print({"page": i})
            if not self.get_pagination(content):
                break
            yield content

    def get_payload(self) -> None:
        start = time.monotonic()
        i = 0
        data = {}
        for content in self.get_pages():
            soup = BeautifulSoup(content, "lxml")
            hrefs = (
                soup.find(class_="css-1ek5um8")
                .find(class_="css-1nvt13t")
                .find("form")
                .find_all(class_="css-1d90tha")[-1]
                .find(class_="css-d4ctjd")
                .find(class_="css-oukcj3")
                .find_all(class_="css-rc5s2u")
            )
            for _ in hrefs:
                created_at = _.find(class_="css-1a4brun er34gjf0").text
                if self.today in created_at.split(" "):
                    i += 1
                    href = self.domain + _.get("href")
                    # title = _.find(class_="css-16v5mdi er34gjf0").text
                    # price = _.find(class_="css-10b0gli er34gjf0").text
                    data[f"{i}"] = {
                        # "title": title,
                        # "price": price,
                        "href": href,
                        "created_at": created_at,
                    }
        names_list = self.uri.split("/")
        self.file_name = f"{names_list[-3]}_{names_list[-2]}"
        with open(f"olx/data/{self.file_name}.json", "w+", encoding="utf-8") as file:
            json.dump(data, file, indent=4, ensure_ascii=False)

class Dopomoga(ScrapingOlx):
    def get_potribna_dopomoha(self):
        self.uri = self.dopomoga_list_uri[0]
        self.get_payload()

    def get_proponuiu_dopomohu(self):
        self.uri = self.dopomoga_list_uri[1]
        self.get_payload()


class Transports(ScrapingOlx):
    def get_legkovye_avtomobili(self):
        self.uri = self.transport_list_uri[0]
        self.get_payload()

    def get_spetstehnika(self):
        self.uri = self.transport_list_uri[1]
        self.get_payload()

    def get_pritsepy_doma_na_kolesah(self):
        self.uri = self.transport_list_uri[2]
        self.get_payload()

    def get_gruzovye_avtomobili(self):
        self.uri = self.transport_list_uri[3]
        self.get_payload()

    def get_selhoztehnika(self):
        self.uri = self.transport_list_uri[4]
        self.get_payload()

    def get_gruzoviki_i_spetstehnika_iz_polshi(self):
        self.uri = self.transport_list_uri[5]
        self.get_payload()

    def get_avtobusy(self):
        self.uri = self.transport_list_uri[6]
        self.get_payload()

    def get_vodnyy_transport(self):
        self.uri = self.transport_list_uri[7]
        self.get_payload()

    def get_drugoy_transport(self):
        self.uri = self.transport_list_uri[8]
        self.get_payload()

    def get_moto(self):
        self.uri = self.transport_list_uri[9]
        self.get_payload()

    def get_avtomobili_iz_polshi(self):
        self.uri = self.transport_list_uri[10]
        self.get_payload()


class DetskiyMir(ScrapingOlx):
    def get_detskaya_odezhda(self):
        self.uri = self.detskiy_mir_list_uri[0]
        self.get_payload()

    def get_detskaya_mebel(self):
        self.uri = self.detskiy_mir_list_uri[1]
        self.get_payload()

    def get_tovary_dlya_shkolnikov(self):
        self.uri = self.detskiy_mir_list_uri[2]
        self.get_payload()

    def get_detskaya_obuv(self):
        self.uri = self.detskiy_mir_list_uri[3]
        self.get_payload()

    def get_igrushki(self):
        self.uri = self.detskiy_mir_list_uri[4]
        self.get_payload()

    def get_prochie_detskie_tovary(self):
        self.uri = self.detskiy_mir_list_uri[5]
        self.get_payload()

    def get_detskie_kolyaski(self):
        self.uri = self.detskiy_mir_list_uri[6]
        self.get_payload()

    def get_detskiy_transport(self):
        self.uri = self.detskiy_mir_list_uri[7]
        self.get_payload()

    def get_detskie_avtokresla(self):
        self.uri = self.detskiy_mir_list_uri[8]
        self.get_payload()

    def get_kormlenie(self):
        self.uri = self.detskiy_mir_list_uri[9]
        self.get_payload()


class Nedvizhimost(ScrapingOlx):
    def get_kvartiry(self):
        self.uri = self.nedvizhimost_list_uri[0]
        self.get_payload()

    def get_zemlya(self):
        self.uri = self.nedvizhimost_list_uri[1]
        self.get_payload()

    def get_komnaty(self):
        self.uri = self.nedvizhimost_list_uri[2]
        self.get_payload()

    def get_kommercheskaya_nedvizhimost(self):
        self.uri = self.nedvizhimost_list_uri[3]
        self.get_payload()

    def get_doma(self):
        self.uri = self.nedvizhimost_list_uri[4]
        self.get_payload()

    def get_garazhy_parkovki(self):
        self.uri = self.nedvizhimost_list_uri[5]
        self.get_payload()

    def get_posutochno_pochasovo(self):
        self.uri = self.nedvizhimost_list_uri[6]
        self.get_payload()

    def get_nedvizhimost_za_rubezhom(self):
        self.uri = self.nedvizhimost_list_uri[7]
        self.get_payload()


class ZapchastiDlyaTransporta(ScrapingOlx):
    def get_avtozapchasti(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[0]
        self.get_payload()

    def get_gps_navigatory_videoregistratory(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[1]
        self.get_payload()

    def get_motoaksessuary(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[2]
        self.get_payload()

    def get_aksessuary_dlya_avto(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[3]
        self.get_payload()

    def get_transport_na_zapchasti_avtorazborki(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[4]
        self.get_payload()

    def get_masla_i_avtokhimiya(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[5]
        self.get_payload()

    def get_avtozvuk_i_multimedia(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[6]
        self.get_payload()

    def get_motozapchasti(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[7]
        self.get_payload()

    def get_prochie_zapchasti(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[8]
        self.get_payload()

    def get_shiny_diski_i_kolesa(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[9]
        self.get_payload()

    def get_motoekipirovka(self):
        self.uri = self.zapchasti_dlya_transporta_list_uri[10]
        self.get_payload()


class Zhivotnye(ScrapingOlx):
    def get_besplatno_zhivotnye_i_vyazka(self):
        self.uri = self.zhivotnye_list_uri[0]
        self.get_payload()

    def get_ptitsy(self):
        self.uri = self.zhivotnye_list_uri[1]
        self.get_payload()

    def get_drugie_zhivotnye(self):
        self.uri = self.zhivotnye_list_uri[2]
        self.get_payload()

    def get_sobaki(self):
        self.uri = self.zhivotnye_list_uri[3]
        self.get_payload()

    def get_gryzuny(self):
        self.uri = self.zhivotnye_list_uri[4]
        self.get_payload()

    def get_tovary_dlya_zhivotnyh(self):
        self.uri = self.zhivotnye_list_uri[5]
        self.get_payload()

    def get_koshki(self):
        self.uri = self.zhivotnye_list_uri[6]
        self.get_payload()

    def get_reptilii(self):
        self.uri = self.zhivotnye_list_uri[7]
        self.get_payload()

    def get_vyazka(self):
        self.uri = self.zhivotnye_list_uri[8]
        self.get_payload()

    def get_akvariumnye_rybki(self):
        self.uri = self.zhivotnye_list_uri[9]
        self.get_payload()

    def get_selskohozyaystvennye_zhivotnye(self):
        self.uri = self.zhivotnye_list_uri[10]
        self.get_payload()

    def get_byuro_nahodok(self):
        self.uri = self.zhivotnye_list_uri[11]
        self.get_payload()


class Elektronika(ScrapingOlx):
    def get_telefony_i_aksesuary(self):
        self.uri = self.elektronika_list_uri[0]
        self.get_payload()

    def get_audiotehnika(self):
        self.uri = self.elektronika_list_uri[1]
        self.get_payload()

    def get_tehnika_dlya_doma(self):
        self.uri = self.elektronika_list_uri[2]
        self.get_payload()

    def get_aksessuary_i_komplektuyuschie(self):
        self.uri = self.elektronika_list_uri[3]
        self.get_payload()

    def get_kompyutery_i_komplektuyuschie(self):
        self.uri = self.elektronika_list_uri[4]
        self.get_payload()

    def get_igry_i_igrovye_pristavki(self):
        self.uri = self.elektronika_list_uri[5]
        self.get_payload()

    def get_tehnika_dlya_kuhni(self):
        self.uri = self.elektronika_list_uri[6]
        self.get_payload()

    def get_prochaja_electronika(self):
        self.uri = self.elektronika_list_uri[7]
        self.get_payload()

    def get_foto_video(self):
        self.uri = self.elektronika_list_uri[8]
        self.get_payload()

    def get_planshety_el_knigi_i_aksessuary(self):
        self.uri = self.elektronika_list_uri[9]
        self.get_payload()

    def get_klimaticheskoe_oborudovanie(self):
        self.uri = self.elektronika_list_uri[10]
        self.get_payload()

    def get_remont_i_obsluzhivanie_tehniki(self):
        self.uri = self.elektronika_list_uri[11]
        self.get_payload()

    def get_tv_videotehnika(self):
        self.uri = self.elektronika_list_uri[12]
        self.get_payload()

    def get_noutbuki_i_aksesuary(self):
        self.uri = self.elektronika_list_uri[13]
        self.get_payload()

    def get_individualnyy_uhod(self):
        self.uri = self.elektronika_list_uri[14]
        self.get_payload()


class DomISad(ScrapingOlx):
    def get_kantstovary_rashodnye_materialy(self):
        self.uri = self.dom_i_sad_list_uri[0]
        self.get_payload()

    def get_predmety_interera(self):
        self.uri = self.dom_i_sad_list_uri[1]
        self.get_payload()

    def get_posuda_kuhonnaya_utvar(self):
        self.uri = self.dom_i_sad_list_uri[2]
        self.get_payload()

    def get_mebel(self):
        self.uri = self.dom_i_sad_list_uri[3]
        self.get_payload()

    def get_stroitelstvo_remont(self):
        self.uri = self.dom_i_sad_list_uri[4]
        self.get_payload()

    def get_sadovyy_inventar(self):
        self.uri = self.dom_i_sad_list_uri[5]
        self.get_payload()

    def get_produkty_pitaniya_napitki(self):
        self.uri = self.dom_i_sad_list_uri[6]
        self.get_payload()

    def get_instrumenty(self):
        self.uri = self.dom_i_sad_list_uri[7]
        self.get_payload()

    def get_hozyaystvennyy_inventar(self):
        self.uri = self.dom_i_sad_list_uri[8]
        self.get_payload()

    def get_sad_ogorod(self):
        self.uri = self.dom_i_sad_list_uri[9]
        self.get_payload()

    def get_komnatnye_rasteniya(self):
        self.uri = self.dom_i_sad_list_uri[10]
        self.get_payload()

    def get_prochie_tovary_dlya_doma(self):
        self.uri = self.dom_i_sad_list_uri[11]
        self.get_payload()


class Rabota(ScrapingOlx):
    def get_roznichnaya_torgovlya_prodazhi_zakupki(self):
        self.uri = self.rabota_list_uri[0]
        self.get_payload()

    def get_upravleniye_personalom_hr(self):
        self.uri = self.rabota_list_uri[1]
        self.get_payload()

    def get_obrazovanie(self):
        self.uri = self.rabota_list_uri[2]
        self.get_payload()

    def get_banki_finansy_strakhovaniye(self):
        self.uri = self.rabota_list_uri[3]
        self.get_payload()

    def get_selskoye_khozyaystvo_agrobiznes_lesnoye_khozyaystvo(self):
        self.uri = self.rabota_list_uri[4]
        self.get_payload()

    def get_buhgalteriya(self):
        self.uri = self.rabota_list_uri[5]
        self.get_payload()

    def get_zsu(self):
        self.uri = self.rabota_list_uri[6]
        self.get_payload()

    def get_transport_logistika(self):
        self.uri = self.rabota_list_uri[7]
        self.get_payload()

    def get_ohrana_bezopasnost(self):
        self.uri = self.rabota_list_uri[8]
        self.get_payload()

    def get_kultura_iskusstvo(self):
        self.uri = self.rabota_list_uri[9]
        self.get_payload()

    def get_nedvizhimost(self):
        self.uri = self.rabota_list_uri[10]
        self.get_payload()

    def get_chastichnaya_zanyatost(self):
        self.uri = self.rabota_list_uri[11]
        self.get_payload()

    def get_otelno_restorannyy_biznes_turizm(self):
        self.uri = self.rabota_list_uri[12]
        self.get_payload()

    def get_stroitelstvo(self):
        self.uri = self.rabota_list_uri[13]
        self.get_payload()

    def get_domashniy_personal(self):
        self.uri = self.rabota_list_uri[14]
        self.get_payload()

    def get_meditsina_farmatsiya(self):
        self.uri = self.rabota_list_uri[15]
        self.get_payload()

    def get_marketing_reklama_dizayn(self):
        self.uri = self.rabota_list_uri[16]
        self.get_payload()

    def get_nachalo_karery_studenty(self):
        self.uri = self.rabota_list_uri[17]
        self.get_payload()

    def get_drugie_sfery_zanyatiy(self):
        self.uri = self.rabota_list_uri[18]
        self.get_payload()

    def get_telekommunikatsii_svyaz(self):
        self.uri = self.rabota_list_uri[19]
        self.get_payload()

    def get_krasota_fitnes_sport(self):
        self.uri = self.rabota_list_uri[20]
        self.get_payload()

    def get_it_telekom_kompyutery(self):
        self.uri = self.rabota_list_uri[21]
        self.get_payload()

    def get_proizvodstvo_energetika(self):
        self.uri = self.rabota_list_uri[22]
        self.get_payload()

    def get_rabota_za_rubezhom(self):
        self.uri = self.rabota_list_uri[23]
        self.get_payload()

    def get_sto_avtomoyki(self):
        self.uri = self.rabota_list_uri[24]
        self.get_payload()


class Uslugi(ScrapingOlx):
    def get_avto_moto_uslugi(self):
        self.uri = self.uslugi_list_uri[0]
        self.get_payload()

    def get_klining(self):
        self.uri = self.uslugi_list_uri[1]
        self.get_payload()

    def get_organizatsiya_prazdnikov(self):
        self.uri = self.uslugi_list_uri[2]
        self.get_payload()

    def get_priem_vtorsyrya(self):
        self.uri = self.uslugi_list_uri[3]
        self.get_payload()

    def get_oborudovanie(self):
        self.uri = self.uslugi_list_uri[4]
        self.get_payload()

    def get_krasota_zdorove(self):
        self.uri = self.uslugi_list_uri[5]
        self.get_payload()

    def get_obrazovanie(self):
        self.uri = self.uslugi_list_uri[6]
        self.get_payload()

    def get_remont_i_obsluzhivanie_tehniki(self):
        self.uri = self.uslugi_list_uri[7]
        self.get_payload()

    def get_turizm_immigratsiya(self):
        self.uri = self.uslugi_list_uri[8]
        self.get_payload()

    def get_uslugi_dlya_zhivotnyh(self):
        self.uri = self.uslugi_list_uri[9]
        self.get_payload()

    def get_nyani_sidelki(self):
        self.uri = self.uslugi_list_uri[10]
        self.get_payload()

    def get_perevozki_arenda_transporta(self):
        self.uri = self.uslugi_list_uri[11]
        self.get_payload()

    def get_stroitelstvo_otdelka_remont(self):
        self.uri = self.uslugi_list_uri[12]
        self.get_payload()

    def get_delovye_uslugi(self):
        self.uri = self.uslugi_list_uri[13]
        self.get_payload()

    def get_ritualnye_uslugi(self):
        self.uri = self.uslugi_list_uri[14]
        self.get_payload()

    def get_bytovye_uslugi(self):
        self.uri = self.uslugi_list_uri[15]
        self.get_payload()

    def get_razvlechenie_foto_video(self):
        self.uri = self.uslugi_list_uri[16]
        self.get_payload()

    def get_syre_materialy(self):
        self.uri = self.uslugi_list_uri[17]
        self.get_payload()

    def get_prodazha_biznesa(self):
        self.uri = self.uslugi_list_uri[18]
        self.get_payload()

    def get_prochie_uslugi(self):
        self.uri = self.uslugi_list_uri[19]
        self.get_payload()


class ArendaProkat(ScrapingOlx):
    def get_transport_spetstehnika(self):
        self.uri = self.arenda_prokat_list_uri[0]
        self.get_payload()

    def get_tovary_med_naznacheniya(self):
        self.uri = self.arenda_prokat_list_uri[1]
        self.get_payload()

    def get_odezhda_aksessuary(self):
        self.uri = self.arenda_prokat_list_uri[2]
        self.get_payload()

    def get_velosipedy_moto(self):
        self.uri = self.arenda_prokat_list_uri[3]
        self.get_payload()

    def get_tehnika_elektronika(self):
        self.uri = self.arenda_prokat_list_uri[4]
        self.get_payload()

    def get_detskaya_odezhda_tovary(self):
        self.uri = self.arenda_prokat_list_uri[5]
        self.get_payload()

    def get_oborudovanie(self):
        self.uri = self.arenda_prokat_list_uri[6]
        self.get_payload()

    def get_tovary_dlya_meropriyatiy(self):
        self.uri = self.arenda_prokat_list_uri[7]
        self.get_payload()

    def get_drugoe(self):
        self.uri = self.arenda_prokat_list_uri[8]
        self.get_payload()

    def get_instrumenty(self):
        self.uri = self.arenda_prokat_list_uri[9]
        self.get_payload()

    def get_sport_turisticheskie_tovary(self):
        self.uri = self.arenda_prokat_list_uri[10]
        self.get_payload()


class ModaIStil(ScrapingOlx):
    def get_zhenskaya_odezhda(self):
        self.uri = self.moda_i_stil_list_uri[0]
        self.get_payload()

    def get_zhenskoe_bele_kupalniki(self):
        self.uri = self.moda_i_stil_list_uri[1]
        self.get_payload()

    def get_naruchnye_chasy(self):
        self.uri = self.moda_i_stil_list_uri[2]
        self.get_payload()

    def get_podarki(self):
        self.uri = self.moda_i_stil_list_uri[3]
        self.get_payload()

    def get_zhenskaya_obuv(self):
        self.uri = self.moda_i_stil_list_uri[4]
        self.get_payload()

    def get_muzhskoe_bele_plavki(self):
        self.uri = self.moda_i_stil_list_uri[5]
        self.get_payload()

    def get_aksessuary(self):
        self.uri = self.moda_i_stil_list_uri[6]
        self.get_payload()

    def get_spetsodezhda_i_spetsobuv(self):
        self.uri = self.moda_i_stil_list_uri[7]
        self.get_payload()

    def get_muzhskaya_odezhda(self):
        self.uri = self.moda_i_stil_list_uri[8]
        self.get_payload()

    def get_golovnye_ubory(self):
        self.uri = self.moda_i_stil_list_uri[9]
        self.get_payload()

    def get_odezhda_dlya_beremennyh(self):
        self.uri = self.moda_i_stil_list_uri[10]
        self.get_payload()

    def get_moda_raznoe(self):
        self.uri = self.moda_i_stil_list_uri[11]
        self.get_payload()

    def get_muzhskaya_obuv(self):
        self.uri = self.moda_i_stil_list_uri[12]
        self.get_payload()

    def get_dlya_svadby(self):
        self.uri = self.moda_i_stil_list_uri[13]
        self.get_payload()

    def get_krasota_zdorove(self):
        self.uri = self.moda_i_stil_list_uri[14]
        self.get_payload()


class HobbiOtdyhISport(ScrapingOlx):
    def get_antikvariat_kollektsii(self):
        self.uri = self.hobbi_list_uri[0]
        self.get_payload()

    def get_militariya(self):
        self.uri = self.hobbi_list_uri[1]
        self.get_payload()

    def get_bilety(self):
        self.uri = self.hobbi_list_uri[2]
        self.get_payload()

    def get_muzykalnye_instrumenty(self):
        self.uri = self.hobbi_list_uri[3]
        self.get_payload()

    def get_kvadrokoptery_i_aksessuary(self):
        self.uri = self.hobbi_list_uri[4]
        self.get_payload()

    def get_poisk_poputchikov(self):
        self.uri = self.hobbi_list_uri[5]
        self.get_payload()

    def get_sport_otdyh(self):
        self.uri = self.hobbi_list_uri[6]
        self.get_payload()

    def get_knigi_zhurnaly(self):
        self.uri = self.hobbi_list_uri[7]
        self.get_payload()

    def get_poisk_grupp_muzykantov(self):
        self.uri = self.hobbi_list_uri[8]
        self.get_payload()

    def get_velo(self):
        self.uri = self.hobbi_list_uri[9]
        self.get_payload()

    def get_cd_dvd_plastinki(self):
        self.uri = self.hobbi_list_uri[10]
        self.get_payload()

    def get_drugoe(self):
        self.uri = self.hobbi_list_uri[11]
        self.get_payload()


class OtdamDarom(ScrapingOlx):
    def get_otdam_darom(self):
        self.uri = self.otdam_darom_list_uri[0]
        self.get_payload()


class ObmenBarter(ScrapingOlx):
    def get_obmen_barter(self):
        self.uri = self.obmen_list_uri[0]
        self.get_payload()
