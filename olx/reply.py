from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

start_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="DOPOMOGA"),
            KeyboardButton(text="TRANSPORTS"),
            KeyboardButton(text="DETSKIY MIR"),
            KeyboardButton(text="NEDVIZHIMOST"),
        ],
        [
            KeyboardButton(text="ZAPCHASTI DLYA TRNSPORTA"),
            KeyboardButton(text="ZHIVOTNYE"),
            KeyboardButton(text="ELEKTRONIKA"),
            KeyboardButton(text="DOM I SAD"),
        ],
        [
            KeyboardButton(text="RABOTA"),
            KeyboardButton(text="USLUGI"),
            KeyboardButton(text="ARENDA PROKAT"),
            KeyboardButton(text="MODA I STIL"),
        ],
        [
            KeyboardButton(text="HOBBI OTDYH I SPORT"),
            KeyboardButton(text="OTDAM DAROM"),
            KeyboardButton(text="OBMEN BARTER"),
        ],
    ],
    resize_keyboard=True,
)

dopomoga_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="potribna dopomoha"),
            KeyboardButton(text="proponuiu dopomohu"),
        ],
    ],
    resize_keyboard=True,
)

transports_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="legkovye avtomobili"),
            KeyboardButton(text="spetstehnika"),
            KeyboardButton(text="pritsepy doma na kolesah"),
            KeyboardButton(text="gruzovye avtomobili"),
        ],
        [
            KeyboardButton(text="selhoztehnika"),
            KeyboardButton(text="gruzoviki i spetstehnika iz polshi"),
            KeyboardButton(text="avtobusy"),
            KeyboardButton(text="vodnyy transport"),
        ],
        [
            KeyboardButton(text="drugoy transport"),
            KeyboardButton(text="moto"),
            KeyboardButton(text="avtomobili-iz-polshi"),
        ],
    ],
    resize_keyboard=True,
)

detskiy_mir_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="detskaya odezhda"),
            KeyboardButton(text="detskaya mebel"),
            KeyboardButton(text="tovary dlya shkolnikov"),
            KeyboardButton(text="detskaya obuv"),
            KeyboardButton(text="kormlenie"),
        ],
        [
            KeyboardButton(text="igrushki"),
            KeyboardButton(text="prochie detskie tovary"),
            KeyboardButton(text="detskie kolyaski"),
            KeyboardButton(text="detskiy transport"),
            KeyboardButton(text="detskie avtokresla"),
        ],
    ],
    resize_keyboard=True,
)

nedvizhimost_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="kvartiry"),
            KeyboardButton(text="zemlya"),
            KeyboardButton(text="komnaty"),
            KeyboardButton(text="kommercheskaya nedvizhimost"),
        ],
        [
            KeyboardButton(text="doma"),
            KeyboardButton(text="garazhy parkovki"),
            KeyboardButton(text="posutochno pochasovo"),
            KeyboardButton(text="nedvizhimost za rubezhom"),
        ],
    ],
    resize_keyboard=True,
)

zapchasti_dlya_transporta_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="avtozapchasti"),
            KeyboardButton(text="gps-navigatory-videoregistratory"),
            KeyboardButton(text="motoaksessuary"),
            KeyboardButton(text="aksessuary-dlya-avto"),
        ],
        [
            KeyboardButton(text="transport na zapchasti avtorazborki"),
            KeyboardButton(text="masla i avtokhimiya"),
            KeyboardButton(text="avtozvuk i multimedia"),
            KeyboardButton(text="motozapchasti"),
        ],
        [
            KeyboardButton(text="prochie-zapchasti"),
            KeyboardButton(text="shiny-diski-i-kolesa"),
            KeyboardButton(text="motoekipirovka"),
        ],
    ],
    resize_keyboard=True,
)

zhivotnye_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="besplatno zhivotnye i vyazka"),
            KeyboardButton(text="ptitsy"),
            KeyboardButton(text="drugie zhivotnye"),
            KeyboardButton(text="sobaki"),
        ],
        [
            KeyboardButton(text="gryzuny"),
            KeyboardButton(text="tovary dlya zhivotnyh"),
            KeyboardButton(text="koshki"),
            KeyboardButton(text="reptilii"),
        ],
        [
            KeyboardButton(text="vyazka"),
            KeyboardButton(text="akvariumnye-rybki"),
            KeyboardButton(text="selskohozyaystvennye zhivotnye"),
            KeyboardButton(text="byuro nahodok"),
        ],
    ],
    resize_keyboard=True,
)

elektronika_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="telefony i aksesuary"),
            KeyboardButton(text="audiotehnika"),
            KeyboardButton(text="tehnika dlya doma"),
            KeyboardButton(text="aksessuary i komplektuyuschie"),
        ],
        [
            KeyboardButton(text="kompyutery i komplektuyuschie"),
            KeyboardButton(text="igry i igrovye pristavki"),
            KeyboardButton(text="tehnika dlya kuhni"),
            KeyboardButton(text="prochaja electronika"),
        ],
        [
            KeyboardButton(text="foto video"),
            KeyboardButton(text="planshety el knigi i aksessuary"),
            KeyboardButton(text="klimaticheskoe oborudovanie"),
            KeyboardButton(text="remont i obsluzhivanie tehniki"),
        ],
        [
            KeyboardButton(text="tv videotehnika"),
            KeyboardButton(text="noutbuki i aksesuary"),
            KeyboardButton(text="individualnyy uhod"),
        ],
    ],
    resize_keyboard=True,
)

dom_i_sad_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="kantstovary rashodnye materialy"),
            KeyboardButton(text="predmety interera"),
            KeyboardButton(text="posuda kuhonnaya utvar"),
            KeyboardButton(text="mebel"),
        ],
        [
            KeyboardButton(text="sadovyy inventar"),
            KeyboardButton(text="produkty pitaniya napitki"),
            KeyboardButton(text="instrumenty"),
            KeyboardButton(text="hozyaystvennyy inventar"),
        ],
        [
            KeyboardButton(text="sad ogorod"),
            KeyboardButton(text="komnatnye rasteniya"),
            KeyboardButton(text="prochie tovary dlya doma"),
        ],
    ],
    resize_keyboard=True,
)

rabota_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="roznichnaya torgovlya prodazhi zakupki"),
            KeyboardButton(text="upravleniye personalom hr"),
            KeyboardButton(text="obrazovanie"),
            KeyboardButton(text="banki finansy strakhovaniye"),
        ],
        [
            KeyboardButton(text="selskoye khozyaystvo agrobiznes lesnoye khozyaystvo"),
            KeyboardButton(text="buhgalteriya"),
            KeyboardButton(text="zsu"),
            KeyboardButton(text="transport logistika"),
        ],
        [
            KeyboardButton(text="kultura iskusstvo"),
            KeyboardButton(text="nedvizhimost"),
            KeyboardButton(text="chastichnaya-zanyatost"),
            KeyboardButton(text="otelno-restorannyy-biznes-turizm"),
        ],
        [
            KeyboardButton(text="stroitelstvo"),
            KeyboardButton(text="domashniy personal"),
            KeyboardButton(text="meditsina farmatsiya"),
            KeyboardButton(text="marketing reklama dizayn"),
        ],
        [
            KeyboardButton(text="nachalo-karery-studenty"),
            KeyboardButton(text="drugie sfery zanyatiy"),
            KeyboardButton(text="telekommunikatsii svyaz"),
            KeyboardButton(text="krasota fitnes sport"),
        ],
        [
            KeyboardButton(text="it telekom kompyutery"),
            KeyboardButton(text="proizvodstvo energetika"),
            KeyboardButton(text="rabota za rubezhom"),
            KeyboardButton(text="sto avtomoyki"),
        ],
    ],
    resize_keyboard=True,
)

uslugi_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="avto-moto-uslugi"),
            KeyboardButton(text="klining"),
            KeyboardButton(text="organizatsiya prazdnikov"),
            KeyboardButton(text="priem vtorsyrya"),
        ],
        [
            KeyboardButton(text="oborudovanie"),
            KeyboardButton(text="krasota-zdorove"),
            KeyboardButton(text="obrazovanie"),
            KeyboardButton(text="remont-i-obsluzhivanie-tehniki"),
        ],
        [
            KeyboardButton(text="kultura iskusstvo"),
            KeyboardButton(text="nedvizhimost"),
            KeyboardButton(text="chastichnaya-zanyatost"),
            KeyboardButton(text="otelno-restorannyy-biznes-turizm"),
        ],
        [
            KeyboardButton(text="turizm immigratsiya"),
            KeyboardButton(text="uslugi dlya zhivotnyh"),
            KeyboardButton(text="nyani sidelki"),
            KeyboardButton(text="perevozki-arenda-transporta"),
        ],
        [
            KeyboardButton(text="stroitelstvo otdelka remont"),
            KeyboardButton(text="delovye uslugi"),
            KeyboardButton(text="ritualnye uslugi"),
            KeyboardButton(text="bytovye uslugi"),
        ],
        [
            KeyboardButton(text="razvlechenie foto video"),
            KeyboardButton(text="syre materialy"),
            KeyboardButton(text="prodazha biznesa"),
            KeyboardButton(text="prochie uslugi"),
        ],
    ],
    resize_keyboard=True,
)

arenda_prokat_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="transport spetstehnika"),
            KeyboardButton(text="tovary med naznacheniya"),
            KeyboardButton(text="odezhda aksessuary"),
            KeyboardButton(text="velosipedy moto"),
        ],
        [
            KeyboardButton(text="tehnika elektronika"),
            KeyboardButton(text="detskaya odezhda tovary"),
            KeyboardButton(text="oborudovanie"),
            KeyboardButton(text="tovary dlya meropriyatiy"),
        ],
        [
            KeyboardButton(text="drugoe"),
            KeyboardButton(text="instrumenty"),
            KeyboardButton(text="sport turisticheskie tovary"),
        ],
    ],
    resize_keyboard=True,
)

moda_i_stil_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="zhenskaya odezhda"),
            KeyboardButton(text="zhenskoe bele kupalniki"),
            KeyboardButton(text="naruchnye chasy"),
            KeyboardButton(text="podarki"),
        ],
        [
            KeyboardButton(text="zhenskaya obuv"),
            KeyboardButton(text="muzhskoe bele plavki"),
            KeyboardButton(text="aksessuary"),
            KeyboardButton(text="spetsodezhda i spetsobuv"),
        ],
        [
            KeyboardButton(text="muzhskaya-odezhda"),
            KeyboardButton(text="golovnye ubory"),
            KeyboardButton(text="odezhda dlya beremennyh"),
            KeyboardButton(text="moda raznoe"),
        ],
        [
            KeyboardButton(text="muzhskaya obuv"),
            KeyboardButton(text="dlya-svadby"),
            KeyboardButton(text="krasota zdorove"),
        ],
    ],
    resize_keyboard=True,
)

hobbi_otdyh_i_sport_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="antikvariat-kollektsii"),
            KeyboardButton(text="militariya"),
            KeyboardButton(text="bilety"),
            KeyboardButton(text="muzykalnye instrumenty"),
        ],
        [
            KeyboardButton(text="kvadrokoptery i aksessuary"),
            KeyboardButton(text="poisk poputchikov"),
            KeyboardButton(text="sport otdyh"),
            KeyboardButton(text="knigi-zhurnaly"),
        ],
        [
            KeyboardButton(text="poisk grupp muzykantov"),
            KeyboardButton(text="velo"),
            KeyboardButton(text="cd dvd plastinki"),
            KeyboardButton(text="drugoe"),
        ],
    ],
    resize_keyboard=True,
)

otdam_darom_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="otdam darom"),
        ],
    ],
    resize_keyboard=True,
)

obmen_barter_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="obmen_barter"),
        ],
    ],
    resize_keyboard=True,
)
