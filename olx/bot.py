import json
from aiogram import Bot, Dispatcher, types, filters, exceptions, F
import asyncio
from dotenv import load_dotenv
import os
import reply
import managers


load_dotenv()

TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
bot = Bot(token=TOKEN)
dp = Dispatcher()


@dp.message(filters.CommandStart())
async def start(message: types.Message) -> None:
    await message.answer("Hey!", reply_markup=reply.start_kb)


### DOPOMOGA ###
olx_dopomoga = managers.Dopomoga()


@dp.message(filters.or_f(filters.Command("DOPOMOGA"), (F.text.lower() == "dopomoga")))
async def base__dopomoga(message: types.Message) -> None:
    await message.answer(olx_dopomoga.answer, reply_markup=reply.dopomoga_kb)


@dp.message(
    filters.or_f(
        filters.Command("potribna dopomoha"), (F.text.lower() == "potribna dopomoha")
    )
)
async def dopomoha__potribna_dopomoha(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dopomoga.get_potribna_dopomoha()
    with open(f"olx/data/{olx_dopomoga.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("proponuiu dopomohu"), (F.text.lower() == "proponuiu dopomohu")
    )
)
async def dopomoha__proponuiu_dopomohu(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dopomoga.get_proponuiu_dopomohu()
    with open(f"olx/data/{olx_dopomoga.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### TRANSPORTS ###
olx_transports = managers.Transports()


@dp.message(
    filters.or_f(filters.Command("TRANSPORTS"), (F.text.lower() == "transports"))
)
async def base__transports(message: types.Message) -> None:
    await message.answer(olx_transports.answer, reply_markup=reply.transports_kb)


@dp.message(
    filters.or_f(
        filters.Command("legkovye avtomobili"),
        (F.text.lower() == "legkovye avtomobili"),
    )
)
async def transports__legkovye_avtomobili(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_legkovye_avtomobili()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("spetstehnika"), (F.text.lower() == "spetstehnika"))
)
async def transports__spetstehnika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_spetstehnika()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("pritsepy doma na kolesah"),
        (F.text.lower() == "pritsepy doma na kolesah"),
    )
)
async def transports__pritsepy_doma_na_kolesah(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_pritsepy_doma_na_kolesah()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("gruzovye avtomobili"),
        (F.text.lower() == "gruzovye avtomobili"),
    )
)
async def transports__gruzovye_avtomobili(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_gruzovye_avtomobili()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("selhoztehnika"), (F.text.lower() == "selhoztehnika"))
)
async def transports__selhoztehnika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_selhoztehnika()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("gruzoviki i spetstehnika iz polshi"),
        (F.text.lower() == "gruzoviki i spetstehnika iz polshi"),
    )
)
async def transports__gruzoviki_i_spetstehnika_iz_polshi(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_transports.get_gruzoviki_i_spetstehnika_iz_polshi()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("avtobusy"), (F.text.lower() == "avtobusy")))
async def transports__avtobusy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_avtobusy()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("vodnyy transport"), (F.text.lower() == "vodnyy transport")
    )
)
async def transports__vodnyy_transport(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_vodnyy_transport()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("drugoy transport"), (F.text.lower() == "drugoy transport")
    )
)
async def transports__drugoy_transport(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_drugoy_transport()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("moto"), (F.text.lower() == "moto")))
async def transports__moto(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_moto()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("avtomobili iz polshi"),
        (F.text.lower() == "avtomobili iz polshi"),
    )
)
async def transports__avtomobili_iz_polshi(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_transports.get_avtomobili_iz_polshi()
    with open(
        f"olx/data/{olx_transports.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### DETSKIY MIR ###
olx_detskiy_mir = managers.DetskiyMir()


@dp.message(
    filters.or_f(filters.Command("DETSKIY MIR"), (F.text.lower() == "detskiy mir"))
)
async def base__detskiy_mir(message: types.Message) -> None:
    await message.answer(olx_detskiy_mir.answer, reply_markup=reply.detskiy_mir_kb)


@dp.message(
    filters.or_f(
        filters.Command("detskaya odezhda"), (F.text.lower() == "detskaya odezhda")
    )
)
async def detskiy_mir__detskaya_odezhda(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_detskaya_odezhda()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("detskaya mebel"), (F.text.lower() == "detskaya mebel")
    )
)
async def detskiy_mir__detskaya_mebel(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_detskaya_mebel()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tovary dlya shkolnikov"),
        (F.text.lower() == "tovary dlya shkolnikov"),
    )
)
async def detskiy_mir__tovary_dlya_shkolnikov(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_tovary_dlya_shkolnikov()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("detskaya obuv"), (F.text.lower() == "detskaya obuv"))
)
async def detskiy_mir__detskaya_obuv(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_detskaya_obuv()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("igrushki"), (F.text.lower() == "igrushki")))
async def detskiy_mir__igrushki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_igrushki()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("prochie detskie tovary"),
        (F.text.lower() == "prochie detskie tovary"),
    )
)
async def detskiy_mir__prochie_detskie_tovary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_prochie_detskie_tovary()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("detskie kolyaski"), (F.text.lower() == "detskie kolyaski")
    )
)
async def detskiy_mir__detskie_kolyaski(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_detskie_kolyaski()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("detskiy transport"), (F.text.lower() == "detskiy transport")
    )
)
async def detskiy_mir__detskiy_transport(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_detskiy_transport()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("detskie avtokresla"), (F.text.lower() == "detskie avtokreslat")
    )
)
async def detskiy_mir__detskie_avtokresla(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_detskie_avtokresla()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("kormlenie"), (F.text.lower() == "kormlenie")))
async def detskiy_mir__kormlenie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_detskiy_mir.get_kormlenie()
    with open(
        f"olx/data/{olx_detskiy_mir.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### NEDVIZHIMOST ###
olx_nedvizhimost = managers.Nedvizhimost()


@dp.message(
    filters.or_f(filters.Command("NEDVIZHIMOST"), (F.text.lower() == "nedvizhimost"))
)
async def base__nedvizhimost(message: types.Message) -> None:
    await message.answer(olx_nedvizhimost.answer, reply_markup=reply.nedvizhimost_kb)


@dp.message(filters.or_f(filters.Command("kvartiry"), (F.text.lower() == "kvartiry")))
async def nedvizhimost__kvartiry(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_kvartiry()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("zemlya"), (F.text.lower() == "zemlya")))
async def nedvizhimost__zemlya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_zemlya()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("komnaty"), (F.text.lower() == "komnaty")))
async def nedvizhimost__komnaty(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_komnaty()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("kommercheskaya nedvizhimost"),
        (F.text.lower() == "kommercheskaya nedvizhimost"),
    )
)
async def nedvizhimost__kommercheskaya_nedvizhimost(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_kommercheskaya_nedvizhimost()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("doma"), (F.text.lower() == "doma")))
async def nedvizhimost__doma(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_doma()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("garazhy parkovki"), (F.text.lower() == "garazhy parkovki")
    )
)
async def nedvizhimost__garazhy_parkovki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_garazhy_parkovki()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("posutochno pochasovo"),
        (F.text.lower() == "posutochno pochasovo"),
    )
)
async def nedvizhimost__posutochno_pochasovo(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_posutochno_pochasovo()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("nedvizhimost za rubezhom"),
        (F.text.lower() == "nedvizhimost za rubezhom"),
    )
)
async def nedvizhimost__nedvizhimost_za_rubezhom(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_nedvizhimost.get_nedvizhimost_za_rubezhom()
    with open(
        f"olx/data/{olx_nedvizhimost.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### ZAPCHASTI DLYA TRNSPORTA ###
olx_zapchasti_dlya_transporta = managers.ZapchastiDlyaTransporta()


@dp.message(
    filters.or_f(
        filters.Command("ZAPCHASTI DLYA TRNSPORTA"),
        (F.text.lower() == "zapchasti dlya transporta"),
    )
)
async def base__zapchasti_dlya_transporta(message: types.Message) -> None:
    await message.answer(
        olx_zapchasti_dlya_transporta.answer,
        reply_markup=reply.zapchasti_dlya_transporta_kb,
    )


@dp.message(
    filters.or_f(filters.Command("avtozapchasti"), (F.text.lower() == "avtozapchasti"))
)
async def zapchasti_dlya_transporta__avtozapchasti(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_avtozapchasti()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("gps navigatory videoregistratory"),
        (F.text.lower() == "gps-navigatory videoregistratory"),
    )
)
async def zapchasti_dlya_transporta__gps_navigatory_videoregistratory(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_gps_navigatory_videoregistratory()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("motoaksessuary"), (F.text.lower() == "motoaksessuary")
    )
)
async def zapchasti_dlya_transporta__motoaksessuary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_motoaksessuary()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("aksessuary dlya avto"),
        (F.text.lower() == "aksessuary-dlya-avto"),
    )
)
async def zapchasti_dlya_transporta__aksessuary_dlya_avto(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_aksessuary_dlya_avto()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("transport na zapchasti avtorazborki"),
        (F.text.lower() == "transport na zapchasti avtorazborki"),
    )
)
async def zapchasti_dlya_transporta__transport_na_zapchasti_avtorazborki(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_transport_na_zapchasti_avtorazborki()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("masla i avtokhimiya"),
        (F.text.lower() == "masla i avtokhimiya"),
    )
)
async def zapchasti_dlya_transporta__masla_i_avtokhimiya(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_masla_i_avtokhimiya()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("avtozvuk i multimedia"),
        (F.text.lower() == "avtozvuk i multimedia"),
    )
)
async def zapchasti_dlya_transporta__avtozvuk_i_multimedia(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_avtozvuk_i_multimedia()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("motozapchasti"), (F.text.lower() == "motozapchasti"))
)
async def zapchasti_dlya_transporta__motozapchasti(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_motozapchasti()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("prochie zapchasti"), (F.text.lower() == "prochie zapchasti")
    )
)
async def zapchasti_dlya_transporta__prochie_zapchasti(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_prochie_zapchasti()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("shiny diski i kolesa"),
        (F.text.lower() == "shiny diski i kolesa"),
    )
)
async def zapchasti_dlya_transporta__shiny_diski_i_kolesa(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_shiny_diski_i_kolesa()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("motoekipirovka"), (F.text.lower() == "motoekipirovka")
    )
)
async def zapchasti_dlya_transporta__motoekipirovka(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zapchasti_dlya_transporta.get_motoekipirovka()
    with open(
        f"olx/data/{olx_zapchasti_dlya_transporta.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### ZHIVOTNYE ###
olx_zhivotnye = managers.Zhivotnye()


@dp.message(filters.or_f(filters.Command("ZHIVOTNYE"), (F.text.lower() == "zhivotnye")))
async def base__zhivotnye(message: types.Message) -> None:
    await message.answer(olx_zhivotnye.answer, reply_markup=reply.zhivotnye_kb)


@dp.message(
    filters.or_f(
        filters.Command("besplatno zhivotnye i vyazka"),
        (F.text.lower() == "besplatno zhivotnye i vyazka"),
    )
)
async def zhivotnye__besplatno_zhivotnye_i_vyazka(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_besplatno_zhivotnye_i_vyazka()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("ptitsy"), (F.text.lower() == "ptitsy")))
async def zhivotnye__ptitsy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_ptitsy()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("drugie zhivotnye"), (F.text.lower() == "drugie zhivotnye")
    )
)
async def zhivotnye__drugie_zhivotnye(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_drugie_zhivotnye()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("sobaki"), (F.text.lower() == "sobaki")))
async def zhivotnye__sobaki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_sobaki()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("gryzuny"), (F.text.lower() == "gryzuny")))
async def zhivotnye__gryzuny(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_gryzuny()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tovary dlya zhivotnyh"),
        (F.text.lower() == "tovary dlya zhivotnyh"),
    )
)
async def zhivotnye__tovary_dlya_zhivotnyh(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_tovary_dlya_zhivotnyh()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("koshki"), (F.text.lower() == "koshki")))
async def zhivotnye__koshki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_koshki()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("reptilii"), (F.text.lower() == "reptilii")))
async def zhivotnye__reptilii(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_reptilii()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("vyazka"), (F.text.lower() == "vyazka")))
async def zhivotnye__vyazka(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_vyazka()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("akvariumnye rybki"), (F.text.lower() == "akvariumnye rybki")
    )
)
async def zhivotnye__akvariumnye_rybki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_akvariumnye_rybki()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("selskohozyaystvennye zhivotnye"),
        (F.text.lower() == "selskohozyaystvennye zhivotnye"),
    )
)
async def zhivotnye__selskohozyaystvennye_zhivotnye(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_selskohozyaystvennye_zhivotnye()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("byuro nahodok"), (F.text.lower() == "byuro nahodok"))
)
async def zhivotnye__byuro_nahodok(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_zhivotnye.get_byuro_nahodok()
    with open(
        f"olx/data/{olx_zhivotnye.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### ELEKTRONIKA ###
olx_elektronika = managers.Elektronika()


@dp.message(
    filters.or_f(filters.Command("ELEKTRONIKA"), (F.text.lower() == "elektronika"))
)
async def base__elektronika(message: types.Message) -> None:
    await message.answer(olx_elektronika.answer, reply_markup=reply.elektronika_kb)


@dp.message(
    filters.or_f(
        filters.Command("telefony i aksesuary"),
        (F.text.lower() == "telefony i aksesuary"),
    )
)
async def elektronika__telefony_i_aksesuary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_telefony_i_aksesuary()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("audiotehnika"), (F.text.lower() == "audiotehnika"))
)
async def elektronika__audiotehnika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_audiotehnika()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tehnika dlya doma"), (F.text.lower() == "tehnika dlya doma")
    )
)
async def elektronika__tehnika_dlya_doma(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_tehnika_dlya_doma()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("aksessuary i komplektuyuschie"),
        (F.text.lower() == "aksessuary i komplektuyuschie"),
    )
)
async def elektronika__aksessuary_i_komplektuyuschie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_aksessuary_i_komplektuyuschie()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("kompyutery i komplektuyuschie"),
        (F.text.lower() == "kompyutery i komplektuyuschie"),
    )
)
async def elektronika__kompyutery_i_komplektuyuschie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_kompyutery_i_komplektuyuschie()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("igry i igrovye pristavki"),
        (F.text.lower() == "igry i igrovye pristavki"),
    )
)
async def elektronika__igry_i_igrovye_pristavki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_igry_i_igrovye_pristavki()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tehnika dlya kuhni"), (F.text.lower() == "tehnika dlya kuhni")
    )
)
async def elektronika__tehnika_dlya_kuhni(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_tehnika_dlya_kuhni()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("prochaja electronika"),
        (F.text.lower() == "prochaja electronika"),
    )
)
async def elektronika__prochaja_electronika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_prochaja_electronika()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("foto video"), (F.text.lower() == "foto video"))
)
async def elektronika__foto_video(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_foto_video()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("planshety el knigi i aksessuary"),
        (F.text.lower() == "planshety el knigi i aksessuary"),
    )
)
async def elektronika__planshety_el_knigi_i_aksessuary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_planshety_el_knigi_i_aksessuary()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("klimaticheskoe oborudovanie"),
        (F.text.lower() == "klimaticheskoe oborudovanie"),
    )
)
async def elektronika__klimaticheskoe_oborudovanie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_klimaticheskoe_oborudovanie()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("remont i obsluzhivanie tehniki"),
        (F.text.lower() == "remont i obsluzhivanie tehniki"),
    )
)
async def elektronika__remont_i_obsluzhivanie_tehniki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_remont_i_obsluzhivanie_tehniki()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tv videotehnika"), (F.text.lower() == "tv videotehnika")
    )
)
async def elektronika__tv_videotehnika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_tv_videotehnika()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("noutbuki i aksesuary"),
        (F.text.lower() == "noutbuki i aksesuary"),
    )
)
async def elektronika__noutbuki_i_aksesuary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_noutbuki_i_aksesuary()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("individualnyy uhod"), (F.text.lower() == "individualnyy uhod")
    )
)
async def elektronika__individualnyy_uhod(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_elektronika.get_individualnyy_uhod()
    with open(
        f"olx/data/{olx_elektronika.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### DOM I SAD ###
olx_dom_i_sad = managers.DomISad()


@dp.message(filters.or_f(filters.Command("DOM I SAD"), (F.text.lower() == "dom i sad")))
async def base__dom_i_sad(message: types.Message) -> None:
    await message.answer(olx_dom_i_sad.answer, reply_markup=reply.dom_i_sad_kb)


@dp.message(
    filters.or_f(
        filters.Command("kantstovary rashodnye materialy"),
        (F.text.lower() == "kantstovary rashodnye materialy"),
    )
)
async def dom_i_sad__kantstovary_rashodnye_materialy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_kantstovary_rashodnye_materialy()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("predmety interera"), (F.text.lower() == "predmety interera")
    )
)
async def dom_i_sad__predmety_interera(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_predmety_interera()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("posuda kuhonnaya utvar"),
        (F.text.lower() == "posuda kuhonnaya utvar"),
    )
)
async def dom_i_sad__posuda_kuhonnaya_utvar(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_posuda_kuhonnaya_utvar()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("mebel"), (F.text.lower() == "mebel")))
async def dom_i_sad__mebel(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_mebel()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("stroitelstvo remont"),
        (F.text.lower() == "stroitelstvo remont"),
    )
)
async def dom_i_sad__stroitelstvo_remont(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_stroitelstvo_remont()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("sadovyy inventar"), (F.text.lower() == "sadovyy inventar")
    )
)
async def dom_i_sad__sadovyy_inventar(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_sadovyy_inventar()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("produkty pitaniya napitki"),
        (F.text.lower() == "produkty pitaniya napitki"),
    )
)
async def dom_i_sad__produkty_pitaniya_napitki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_produkty_pitaniya_napitki()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("instrumenty"), (F.text.lower() == "instrumenty"))
)
async def dom_i_sad__instrumenty(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_instrumenty()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("hozyaystvennyy inventar"),
        (F.text.lower() == "hozyaystvennyy inventar"),
    )
)
async def dom_i_sad__hozyaystvennyy_inventar(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_hozyaystvennyy_inventar()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("sad ogorod"), (F.text.lower() == "sad ogorod"))
)
async def dom_i_sad__sad_ogorod(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_sad_ogorod()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("komnatnye rasteniya"),
        (F.text.lower() == "komnatnye rasteniya"),
    )
)
async def dom_i_sad__komnatnye_rasteniya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_komnatnye_rasteniya()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("prochie tovary dlya doma"),
        (F.text.lower() == "prochie tovary dlya doma"),
    )
)
async def dom_i_sad__prochie_tovary_dlya_doma(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_dom_i_sad.get_prochie_tovary_dlya_doma()
    with open(
        f"olx/data/{olx_dom_i_sad.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### RABOTA ###
olx_rabota = managers.Rabota()


@dp.message(filters.or_f(filters.Command("RABOTA"), (F.text.lower() == "rabota")))
async def base__rabota(message: types.Message) -> None:
    await message.answer(olx_rabota.answer, reply_markup=reply.rabota_kb)


@dp.message(
    filters.or_f(
        filters.Command("roznichnaya torgovlya prodazhi zakupki"),
        (F.text.lower() == "roznichnaya torgovlya prodazhi zakupki"),
    )
)
async def rabota__roznichnaya_torgovlya_prodazhi_zakupki(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_roznichnaya_torgovlya_prodazhi_zakupki()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("upravleniye personalom hr"),
        (F.text.lower() == "upravleniye personalom hr"),
    )
)
async def rabota__upravleniye_personalom_hr(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_upravleniye_personalom_hr()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("obrazovanie"), (F.text.lower() == "obrazovanie"))
)
async def rabota__obrazovanie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_obrazovanie()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("banki finansy strakhovaniye"),
        (F.text.lower() == "banki finansy strakhovaniye"),
    )
)
async def rabota__banki_finansy_strakhovaniye(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_banki_finansy_strakhovaniye()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("selskoye khozyaystvo agrobiznes lesnoye khozyaystvo"),
        (F.text.lower() == "selskoye khozyaystvo agrobiznes lesnoye khozyaystvo"),
    )
)
async def rabota__selskoye_khozyaystvo_agrobiznes_lesnoye_khozyaystvo(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_selskoye_khozyaystvo_agrobiznes_lesnoye_khozyaystvo()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("buhgalteriya"), (F.text.lower() == "buhgalteriya"))
)
async def rabota__buhgalteriya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_buhgalteriya()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("zsu"), (F.text.lower() == "zsu")))
async def rabota__zsu(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_zsu()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("transport logistika"),
        (F.text.lower() == "transport logistika"),
    )
)
async def rabota__transport_logistika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_transport_logistika()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("ohrana bezopasnost"), (F.text.lower() == "ohrana bezopasnost")
    )
)
async def rabota__ohrana_bezopasnost(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_ohrana_bezopasnost()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("kultura iskusstvo"), (F.text.lower() == "kultura iskusstvo")
    )
)
async def rabota__kultura_iskusstvo(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_kultura_iskusstvo()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("nedvizhimost"), (F.text.lower() == "nedvizhimost"))
)
async def rabota__nedvizhimost(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_nedvizhimost()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("chastichnaya zanyatost"),
        (F.text.lower() == "chastichnaya zanyatost"),
    )
)
async def rabota__chastichnaya_zanyatost(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_chastichnaya_zanyatost()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("otelno restorannyy biznes turizm"),
        (F.text.lower() == "otelno restorannyy biznes turizm"),
    )
)
async def rabota__otelno_restorannyy_biznes_turizm(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_otelno_restorannyy_biznes_turizm()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("stroitelstvo"), (F.text.lower() == "stroitelstvo"))
)
async def rabota__stroitelstvo(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_stroitelstvo()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("domashniy personal"), (F.text.lower() == "domashniy personal")
    )
)
async def rabota__domashniy_personal(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_domashniy_personal()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("meditsina farmatsiya"),
        (F.text.lower() == "meditsina farmatsiya"),
    )
)
async def rabota__meditsina_farmatsiya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_meditsina_farmatsiya()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("marketing reklama dizayn"),
        (F.text.lower() == "marketing reklama dizayn"),
    )
)
async def rabota__marketing_reklama_dizayn(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_marketing_reklama_dizayn()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("nachalo karery studenty"),
        (F.text.lower() == "nachalo karery studenty"),
    )
)
async def rabota__nachalo_karery_studenty(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_nachalo_karery_studenty()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("drugie sfery zanyatiy"),
        (F.text.lower() == "drugie sfery zanyatiy"),
    )
)
async def rabota__drugie_sfery_zanyatiy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_drugie_sfery_zanyatiy()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("telekommunikatsii svyaz"),
        (F.text.lower() == "telekommunikatsii svyaz"),
    )
)
async def rabota__telekommunikatsii_svyaz(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_telekommunikatsii_svyaz()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("krasota fitnes sport"),
        (F.text.lower() == "krasota fitnes sport"),
    )
)
async def rabota__krasota_fitnes_sport(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_krasota_fitnes_sport()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("it telekom kompyutery"),
        (F.text.lower() == "it telekom kompyutery"),
    )
)
async def rabota__it_telekom_kompyutery(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_it_telekom_kompyutery()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("proizvodstvo energetika"),
        (F.text.lower() == "proizvodstvo energetika"),
    )
)
async def rabota__proizvodstvo_energetika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_proizvodstvo_energetika()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("rabota za rubezhom"), (F.text.lower() == "rabota za rubezhom")
    )
)
async def rabota__rabota_za_rubezhom(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_rabota_za_rubezhom()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("sto avtomoyki"), (F.text.lower() == "sto avtomoyki"))
)
async def rabota__sto_avtomoyki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_rabota.get_sto_avtomoyki()
    with open(f"olx/data/{olx_rabota.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### USLUGI ###
@dp.message(filters.or_f(filters.Command("USLUGI"), (F.text.lower() == "uslugi")))
async def base__uslugi(message: types.Message) -> None:
    await message.answer("Please choise the category", reply_markup=reply.uslugi_kb)


olx_uslugi = managers.Uslugi()


@dp.message(
    filters.or_f(
        filters.Command("avto moto uslugi"), (F.text.lower() == "avto moto uslugi")
    )
)
async def uslugi__avto_moto_uslugi(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_avto_moto_uslugi()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("klining"), (F.text.lower() == "klining")))
async def uslugi__avto_klining(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_klining()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("organizatsiya prazdnikov"),
        (F.text.lower() == "organizatsiya prazdnikov"),
    )
)
async def uslugi__organizatsiya_prazdnikov(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_organizatsiya_prazdnikov()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("priem vtorsyrya"), (F.text.lower() == "priem vtorsyrya")
    )
)
async def uslugi__priem_vtorsyrya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_priem_vtorsyrya()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("oborudovanie"), (F.text.lower() == "oborudovanie"))
)
async def uslugi__uslugi_oborudovanie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_oborudovanie()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("krasota zdorove"), (F.text.lower() == "krasota zdorove")
    )
)
async def uslugi__krasota_zdorove(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_krasota_zdorove()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("obrazovanie"), (F.text.lower() == "obrazovanie"))
)
async def uslugi__obrazovanie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_obrazovanie()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("remont i obsluzhivanie tehniki"),
        (F.text.lower() == "remont i obsluzhivanie tehniki"),
    )
)
async def uslugi__remont_i_obsluzhivanie_tehniki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_remont_i_obsluzhivanie_tehniki()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("turizm immigratsiya"),
        (F.text.lower() == "turizm immigratsiya"),
    )
)
async def uslugi__turizm_immigratsiya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_turizm_immigratsiya()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("uslugi dlya zhivotnyh"),
        (F.text.lower() == "uslugi dlya zhivotnyh"),
    )
)
async def uslugi__uslugi_dlya_zhivotnyh(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_uslugi_dlya_zhivotnyh()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("nyani sidelki"), (F.text.lower() == "nyani sidelki"))
)
async def uslugi__nyani_sidelki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_nyani_sidelki()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("perevozki arenda transporta"),
        (F.text.lower() == "perevozki arenda transporta"),
    )
)
async def uslugi__perevozki_arenda_transporta(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_perevozki_arenda_transporta()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("stroitelstvo otdelka remont"),
        (F.text.lower() == "stroitelstvo otdelka remont"),
    )
)
async def uslugi__stroitelstvo_otdelka_remont(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_stroitelstvo_otdelka_remont()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("delovye uslugi"), (F.text.lower() == "delovye uslugi")
    )
)
async def uslugi__delovye_uslugi(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_delovye_uslugi()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("ritualnye uslugi"), (F.text.lower() == "ritualnye uslugi")
    )
)
async def uslugi__ritualnye_uslugi(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_ritualnye_uslugi()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("bytovye uslugi"), (F.text.lower() == "bytovye uslugi")
    )
)
async def uslugi__bytovye_uslugi(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_bytovye_uslugi()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("razvlechenie foto video"),
        (F.text.lower() == "razvlechenie foto video"),
    )
)
async def uslugi__razvlechenie_foto_video(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_razvlechenie_foto_video()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("syre materialy"), (F.text.lower() == "syre materialy")
    )
)
async def uslugi__syre_materialy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_syre_materialy()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("prodazha biznesa"), (F.text.lower() == "prodazha biznesa")
    )
)
async def uslugi__prodazha_biznesa(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_prodazha_biznesa()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("prochie uslugi"), (F.text.lower() == "prochie uslugi")
    )
)
async def uslugi__prochie_uslugi(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_uslugi.get_prochie_uslugi()
    with open(f"olx/data/{olx_uslugi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### ARENDA PROKAT ###
olx_arenda_prokat = managers.ArendaProkat()


@dp.message(
    filters.or_f(filters.Command("ARENDA PROKAT"), (F.text.lower() == "arenda_prokat"))
)
async def base__arenda_prokat(message: types.Message) -> None:
    await message.answer(olx_arenda_prokat.answer, reply_markup=reply.arenda_prokat_kb)


@dp.message(
    filters.or_f(
        filters.Command("transport spetstehnika"),
        (F.text.lower() == "transport spetstehnika"),
    )
)
async def arenda_prokat__transport_spetstehnika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_transport_spetstehnika()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tovary med naznacheniya"),
        (F.text.lower() == "tovary med naznacheniya"),
    )
)
async def arenda_prokat__tovary_med_naznacheniya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_tovary_med_naznacheniya()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("odezhda aksessuary"), (F.text.lower() == "odezhda aksessuary")
    )
)
async def arenda_prokat__odezhda_aksessuary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_odezhda_aksessuary()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("velosipedy moto"), (F.text.lower() == "velosipedy moto")
    )
)
async def arenda_prokat__velosipedy_moto(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_velosipedy_moto()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tehnika elektronika"),
        (F.text.lower() == "tehnika elektronika"),
    )
)
async def arenda_prokat__tehnika_elektronika(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_tehnika_elektronika()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("detskaya odezhda tovary"),
        (F.text.lower() == "detskaya odezhda tovary"),
    )
)
async def arenda_prokat__detskaya_odezhda_tovary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_detskaya_odezhda_tovary()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("oborudovanie"), (F.text.lower() == "oborudovanie"))
)
async def arenda_prokat__oborudovanie(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_oborudovanie()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("tovary dlya meropriyatiy"),
        (F.text.lower() == "tovary dlya meropriyatiy"),
    )
)
async def arenda_prokat__tovary_dlya_meropriyatiy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_tovary_dlya_meropriyatiy()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("drugoe"), (F.text.lower() == "drugoe")))
async def arenda_prokat__drugoe(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_drugoe()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("instrumenty"), (F.text.lower() == "instrumenty"))
)
async def arenda_prokat__instrumenty(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_instrumenty()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("sport turisticheskie tovary"),
        (F.text.lower() == "sport turisticheskie tovary"),
    )
)
async def arenda_prokat__sport_turisticheskie_tovary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_arenda_prokat.get_sport_turisticheskie_tovary()
    with open(
        f"olx/data/{olx_arenda_prokat.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### MODA I STIL ###
olx_moda_i_stil = managers.ModaIStil()


@dp.message(
    filters.or_f(filters.Command("MODA I STIL"), (F.text.lower() == "moda i stil"))
)
async def base__moda_i_stil(message: types.Message) -> None:
    await message.answer(olx_moda_i_stil.answer, reply_markup=reply.moda_i_stil_kb)


@dp.message(
    filters.or_f(
        filters.Command("zhenskaya odezhda"), (F.text.lower() == "zhenskaya odezhda")
    )
)
async def moda_i_stil__zhenskaya_odezhda(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_zhenskaya_odezhda()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("zhenskoe bele kupalniki"),
        (F.text.lower() == "zhenskoe bele kupalniki"),
    )
)
async def moda_i_stil__zhenskoe_bele_kupalniki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_zhenskoe_bele_kupalniki()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("naruchnye chasy"), (F.text.lower() == "naruchnye chasy")
    )
)
async def moda_i_stil__naruchnye_chasy(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_naruchnye_chasy()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("podarki"), (F.text.lower() == "podarki")))
async def moda_i_stil__podarki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_podarki()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("zhenskaya obuv"), (F.text.lower() == "zhenskaya obuv")
    )
)
async def moda_i_stil__zhenskaya_obuv(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_zhenskaya_obuv()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("muzhskoe bele plavki"),
        (F.text.lower() == "muzhskoe bele plavki"),
    )
)
async def moda_i_stil__muzhskoe_bele_plavki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_muzhskoe_bele_plavki()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("aksessuary"), (F.text.lower() == "aksessuary"))
)
async def moda_i_stil__aksessuary(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_aksessuary()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("spetsodezhda i spetsobuv"),
        (F.text.lower() == "spetsodezhda i spetsobuv"),
    )
)
async def moda_i_stil__spetsodezhda_i_spetsobuv(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_spetsodezhda_i_spetsobuv()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("muzhskaya odezhda"), (F.text.lower() == "muzhskaya odezhda")
    )
)
async def moda_i_stil__muzhskaya_odezhda(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_muzhskaya_odezhda()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("golovnye ubory"), (F.text.lower() == "golovnye ubory")
    )
)
async def moda_i_stil__golovnye_ubory(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_golovnye_ubory()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("odezhda dlya beremennyh"),
        (F.text.lower() == "odezhda dlya beremennyh"),
    )
)
async def moda_i_stil__odezhda_dlya_beremennyh(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_odezhda_dlya_beremennyh()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("moda raznoe"), (F.text.lower() == "moda raznoe"))
)
async def moda_i_stil__moda_raznoe(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_moda_raznoe()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("muzhskaya obuv"), (F.text.lower() == "muzhskaya obuv")
    )
)
async def moda_i_stil__muzhskaya_obuv(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_muzhskaya_obuv()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("dlya svadby"), (F.text.lower() == "dlya svadby"))
)
async def moda_i_stil__dlya_svadby(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_dlya_svadby()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("krasota zdorove"), (F.text.lower() == "krasota zdorove")
    )
)
async def moda_i_stil__krasota_zdorove(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_moda_i_stil.get_krasota_zdorove()
    with open(
        f"olx/data/{olx_moda_i_stil.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### HOBBI OTDYH I SPORT ###
olx_hobbi = managers.HobbiOtdyhISport()


@dp.message(
    filters.or_f(
        filters.Command("HOBBI OTDYH I SPORT"),
        (F.text.lower() == "hobbi otdyh i sport"),
    )
)
async def bse__hobbi_otdyh_i_sport(message: types.Message) -> None:
    await message.answer(olx_hobbi.answer, reply_markup=reply.hobbi_otdyh_i_sport_kb)


@dp.message(
    filters.or_f(
        filters.Command("antikvariat kollektsii"),
        (F.text.lower() == "antikvariat kollektsii"),
    )
)
async def hobbi_otdyh_i_sport__antikvariat_kollektsii(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_antikvariat_kollektsii()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("militariya"), (F.text.lower() == "militariya"))
)
async def hobbi_otdyh_i_sport__militariya(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_militariya()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("bilety"), (F.text.lower() == "bilety")))
async def hobbi_otdyh_i_sport__bilety(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_bilety()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("muzykalnye instrumenty"),
        (F.text.lower() == "muzykalnye instrumenty"),
    )
)
async def hobbi_otdyh_i_sport__muzykalnye_instrumenty(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_muzykalnye_instrumenty()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("kvadrokoptery i aksessuary"),
        (F.text.lower() == "kvadrokoptery i aksessuary"),
    )
)
async def hobbi_otdyh_i_sport__kvadrokoptery_i_aksessuary(
    message: types.Message,
) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_kvadrokoptery_i_aksessuary()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("poisk poputchikov"), (F.text.lower() == "poisk_poputchikov")
    )
)
async def hobbi_otdyh_i_sport__poisk_poputchikov(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_poisk_poputchikov()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(filters.Command("sport otdyh"), (F.text.lower() == "sport otdyh"))
)
async def hobbi_otdyh_i_sport__sport_otdyh(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_sport_otdyh()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("knigi zhurnaly"), (F.text.lower() == "knigi zhurnaly")
    )
)
async def hobbi_otdyh_i_sport__knigi_zhurnaly(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_knigi_zhurnaly()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("poisk grupp muzykantov"),
        (F.text.lower() == "poisk grupp muzykantov"),
    )
)
async def hobbi_otdyh_i_sport__poisk_grupp_muzykantov(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_poisk_grupp_muzykantov()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("velo"), (F.text.lower() == "velo")))
async def hobbi_otdyh_i_sport__velo(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_velo()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(
    filters.or_f(
        filters.Command("cd dvd plastinki"), (F.text.lower() == "cd dvd plastinki")
    )
)
async def hobbi_otdyh_i_sport__cd_dvd_plastinki(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_cd_dvd_plastinki()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


@dp.message(filters.or_f(filters.Command("drugoe"), (F.text.lower() == "drugoe")))
async def hobbi_otdyh_i_sport__drugoe(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_hobbi.get_drugoe()
    with open(f"olx/data/{olx_hobbi.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### OTDAM DAROM ###
olx_otdam_darom = managers.OtdamDarom()


@dp.message(
    filters.or_f(filters.Command("OTDAM DAROM"), (F.text.lower() == "otdam darom"))
)
async def base__otdam_darom(message: types.Message) -> None:
    await message.answer(olx_otdam_darom.answer, reply_markup=reply.otdam_darom_kb)


@dp.message(
    filters.or_f(filters.Command("otdam darom"), (F.text.lower() == "otdam darom"))
)
async def otdam_darom__otdam_darom(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_otdam_darom.get_otdam_darom()
    with open(
        f"olx/data/{olx_otdam_darom.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


### OBMEN BARTER ###
olx_obmen_barter = managers.ObmenBarter()


@dp.message(
    filters.or_f(filters.Command("OBMEN BARTER"), (F.text.lower() == "obmen barter"))
)
async def base__obmen_barter(message: types.Message) -> None:
    await message.answer(olx_obmen_barter.answer, reply_markup=reply.obmen_barter_kb)


@dp.message(
    filters.or_f(filters.Command("obmen barter"), (F.text.lower() == "obmen barter"))
)
async def obmen_barter__obmen_barter(message: types.Message) -> None:
    await message.answer("Please wait...")
    olx_obmen_barter.get_obmen_barter()
    with open(
        f"olx/data/{olx_obmen_barter.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        try:
            await message.answer(str(value.get("href")))
        except exceptions.TelegramRetryAfter:
            continue


async def main() -> None:
    await bot.set_my_commands(
        commands=[types.BotCommand(command="/start", description="Menu")],
        scope=types.BotCommandScopeAllPrivateChats(),
    )
    await dp.start_polling(bot)


asyncio.run(main())
