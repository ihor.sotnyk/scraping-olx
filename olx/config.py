HEADERS = {
    "Dnt": "1",
    "Referer": "https://www.olx.ua/",
    "Sec-Ch-Ua": '"Not_A Brand";v="8", "Chromium";v="120", "Avast Secure Browser";v="120"',
    "Sec-Ch-Ua-Mobile": "?0",
    "Sec-Ch-Ua-Platform": "Windows",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Avast/120.0.0.0",
}

DOMAIN = "https://www.olx.ua"

TODAY = "Сьогодні"

ANSWER = "Please choise the category"

DOPOMOGA = [
    "/uk/dopomoga/potribna_dopomoha/",
    "/uk/dopomoga/proponuiu_dopomohu/",
]

TRANSPORTS = [
    "/uk/transport/legkovye-avtomobili/",
    "/uk/transport/spetstehnika/",
    "/uk/transport/pritsepy-doma-na-kolesah/",
    "/uk/transport/gruzovye-avtomobili/",
    "/uk/transport/selhoztehnika/",
    "/uk/transport/gruzoviki-i-spetstehnika-iz-polshi/",
    "/uk/transport/avtobusy/",
    "/uk/transport/vodnyy-transport/",
    "/uk/transport/drugoy-transport/",
    "/uk/transport/moto/",
    "/uk/transport/avtomobili-iz-polshi/",
]

DETSKIY_MIR = [
    "/uk/detskiy-mir/detskaya-odezhda/",
    "/uk/detskiy-mir/detskaya-mebel/",
    "/uk/detskiy-mir/tovary-dlya-shkolnikov/",
    "/uk/detskiy-mir/detskaya-obuv/",
    "/uk/detskiy-mir/igrushki/",
    "/uk/detskiy-mir/prochie-detskie-tovary/",
    "/uk/detskiy-mir/detskie-kolyaski/",
    "/uk/detskiy-mir/detskiy-transport/",
    "/uk/detskiy-mir/detskie-avtokresla/",
    "/uk/detskiy-mir/kormlenie/",
]

NEDVIZHIMOST = [
    "/uk/nedvizhimost/kvartiry/",
    "/uk/nedvizhimost/zemlya/",
    "/uk/nedvizhimost/komnaty/",
    "/uk/nedvizhimost/kommercheskaya-nedvizhimost/",
    "/uk/nedvizhimost/doma/",
    "/uk/nedvizhimost/garazhy-parkovki/",
    "/uk/nedvizhimost/posutochno-pochasovo/",
    "/uk/nedvizhimost/nedvizhimost-za-rubezhom/",
]

ZAPCHASTI_DLYA_TRNSPORTA = [
    "/uk/zapchasti-dlya-transporta/avtozapchasti/",
    "/uk/zapchasti-dlya-transporta/gps-navigatory-videoregistratory/",
    "/uk/zapchasti-dlya-transporta/motoaksessuary/",
    "/uk/zapchasti-dlya-transporta/aksessuary-dlya-avto/",
    "/uk/zapchasti-dlya-transporta/transport-na-zapchasti-avtorazborki/",
    "/uk/zapchasti-dlya-transporta/masla-i-avtokhimiya/",
    "/uk/zapchasti-dlya-transporta/avtozvuk-i-multimedia/",
    "/uk/zapchasti-dlya-transporta/motozapchasti/",
    "/uk/zapchasti-dlya-transporta/prochie-zapchasti/",
    "/uk/zapchasti-dlya-transporta/shiny-diski-i-kolesa/",
    "/uk/zapchasti-dlya-transporta/motoekipirovka/",
]

ZHIVOTNYE = [
    "/uk/zhivotnye/besplatno-zhivotnye-i-vyazka/",
    "/uk/zhivotnye/ptitsy/",
    "/uk/zhivotnye/drugie-zhivotnye/",
    "/uk/zhivotnye/sobaki/",
    "/uk/zhivotnye/gryzuny/",
    "/uk/zhivotnye/tovary-dlya-zhivotnyh/",
    "/uk/zhivotnye/koshki/",
    "/uk/zhivotnye/reptilii/",
    "/uk/zhivotnye/vyazka/",
    "/uk/zhivotnye/akvariumnye-rybki/",
    "/uk/zhivotnye/selskohozyaystvennye-zhivotnye/",
    "/uk/zhivotnye/byuro-nahodok/",
]

ELEKTRONIKA = [
    "/uk/elektronika/telefony-i-aksesuary/",
    "/uk/elektronika/audiotehnika/",
    "/uk/elektronika/tehnika-dlya-doma/",
    "/uk/elektronika/aksessuary-i-komplektuyuschie/",
    "/uk/elektronika/kompyutery-i-komplektuyuschie/",
    "/uk/elektronika/igry-i-igrovye-pristavki/",
    "/uk/elektronika/tehnika-dlya-kuhni/",
    "/uk/elektronika/prochaja-electronika/",
    "/uk/elektronika/foto-video/",
    "/uk/elektronika/planshety-el-knigi-i-aksessuary/",
    "/uk/elektronika/klimaticheskoe-oborudovanie/",
    "/uk/uslugi/remont-i-obsluzhivanie-tehniki/",
    "/uk/elektronika/tv-videotehnika/",
    "/uk/elektronika/noutbuki-i-aksesuary/",
    "/uk/elektronika/individualnyy-uhod/",
]

DOM_I_SAD = [
    "/uk/dom-i-sad/kantstovary-rashodnye-materialy/",
    "/uk/dom-i-sad/predmety-interera/",
    "/uk/dom-i-sad/posuda-kuhonnaya-utvar/",
    "/uk/dom-i-sad/mebel/",
    "/uk/dom-i-sad/stroitelstvo-remont/",
    "/uk/dom-i-sad/sadovyy-inventar/",
    "/uk/dom-i-sad/produkty-pitaniya-napitki/",
    "/uk/dom-i-sad/instrumenty/",
    "/uk/dom-i-sad/hozyaystvennyy-inventar/",
    "/uk/dom-i-sad/sad-ogorod/",
    "/uk/dom-i-sad/komnatnye-rasteniya/",
    "/uk/dom-i-sad/prochie-tovary-dlya-doma/",
]

RABOTA = [
    "/uk/rabota/roznichnaya-torgovlya-prodazhi-zakupki/",
    "/uk/rabota/upravleniye-personalom-hr/",
    "/uk/rabota/obrazovanie/",
    "/uk/rabota/banki-finansy-strakhovaniye/",
    "/uk/rabota/selskoye-khozyaystvo-agrobiznes-lesnoye-khozyaystvo/",
    "/uk/rabota/buhgalteriya/",
    "/uk/rabota/zsu/",
    "/uk/rabota/transport-logistika/",
    "/uk/rabota/ohrana-bezopasnost/",
    "/uk/rabota/kultura-iskusstvo/",
    "/uk/rabota/nedvizhimost/",
    "/uk/rabota/chastichnaya-zanyatost/",
    "/uk/rabota/otelno-restorannyy-biznes-turizm/",
    "/uk/rabota/stroitelstvo/",
    "/uk/rabota/domashniy-personal/",
    "/uk/rabota/meditsina-farmatsiya/",
    "/uk/rabota/marketing-reklama-dizayn/",
    "/uk/rabota/nachalo-karery-studenty/",
    "/uk/rabota/drugie-sfery-zanyatiy/",
    "/uk/rabota/telekommunikatsii-svyaz/",
    "/uk/rabota/krasota-fitnes-sport/",
    "/uk/rabota/it-telekom-kompyutery/",
    "/uk/rabota/proizvodstvo-energetika/",
    "/uk/rabota/rabota-za-rubezhom/",
    "/uk/rabota/sto-avtomoyki/",
]

USLUGI = [
    "/uk/uslugi/avto-moto-uslugi/",
    "/uk/uslugi/klining/",
    "/uk/uslugi/organizatsiya-prazdnikov/",
    "/uk/uslugi/priem-vtorsyrya/",
    "/uk/uslugi/oborudovanie/",
    "/uk/uslugi/krasota-zdorove/",
    "/uk/uslugi/obrazovanie/",
    "/uk/uslugi/remont-i-obsluzhivanie-tehniki/",
    "/uk/uslugi/turizm-immigratsiya/",
    "/uk/uslugi/uslugi-dlya-zhivotnyh/",
    "/uk/uslugi/nyani-sidelki/",
    "/uk/uslugi/perevozki-arenda-transporta/",
    "/uk/uslugi/stroitelstvo-otdelka-remont/",
    "/uk/uslugi/delovye-uslugi/",
    "/uk/uslugi/ritualnye-uslugi/",
    "/uk/uslugi/bytovye-uslugi/",
    "/uk/uslugi/razvlechenie-foto-video/",
    "/uk/uslugi/syre-materialy/",
    "/uk/uslugi/prodazha-biznesa/",
    "/uk/uslugi/prochie-uslugi/",
]

ARENDA_PROKAT = [
    "/uk/arenda-prokat/transport-spetstehnika/",
    "/uk/arenda-prokat/tovary-med-naznacheniya/",
    "/uk/arenda-prokat/odezhda-aksessuary/",
    "/uk/arenda-prokat/velosipedy-moto/",
    "/uk/arenda-prokat/tehnika-elektronika/",
    "/uk/arenda-prokat/detskaya-odezhda-tovary/",
    "/uk/arenda-prokat/oborudovanie/",
    "/uk/arenda-prokat/tovary-dlya-meropriyatiy/",
    "/uk/arenda-prokat/drugoe/",
    "/uk/arenda-prokat/instrumenty/",
    "/uk/arenda-prokat/sport-turisticheskie-tovary/",
]

MODA_I_STIL = [
    "/uk/moda-i-stil/zhenskaya-odezhda/",
    "/uk/moda-i-stil/zhenskoe-bele-kupalniki/",
    "/uk/moda-i-stil/naruchnye-chasy/",
    "/uk/moda-i-stil/podarki/",
    "/uk/moda-i-stil/zhenskaya-obuv/",
    "/uk/moda-i-stil/muzhskoe-bele-plavki/",
    "/uk/moda-i-stil/aksessuary/",
    "/uk/moda-i-stil/spetsodezhda-i-spetsobuv/",
    "/uk/moda-i-stil/muzhskaya-odezhda/",
    "/uk/moda-i-stil/golovnye-ubory/",
    "/uk/moda-i-stil/odezhda-dlya-beremennyh/",
    "/uk/moda-i-stil/moda-raznoe/",
    "/uk/moda-i-stil/muzhskaya-obuv/",
    "/uk/moda-i-stil/dlya-svadby/",
    "/uk/moda-i-stil/krasota-zdorove/",
]

HOBBI = [
    "/uk/hobbi-otdyh-i-sport/antikvariat-kollektsii/",
    "/uk/hobbi-otdyh-i-sport/militariya/",
    "/uk/hobbi-otdyh-i-sport/bilety/",
    "/uk/hobbi-otdyh-i-sport/muzykalnye-instrumenty/",
    "/uk/hobbi-otdyh-i-sport/kvadrokoptery-i-aksessuary/",
    "/uk/hobbi-otdyh-i-sport/poisk-poputchikov/",
    "/uk/hobbi-otdyh-i-sport/sport-otdyh/",
    "/uk/hobbi-otdyh-i-sport/knigi-zhurnaly/",
    "/uk/hobbi-otdyh-i-sport/poisk-grupp-muzykantov/",
    "/uk/hobbi-otdyh-i-sport/velo/",
    "/uk/hobbi-otdyh-i-sport/cd-dvd-plastinki/",
    "/uk/hobbi-otdyh-i-sport/drugoe/",
]

OTDAM_DAROM = ["/uk/otdam-darom/"]

OBMEN = ["/uk/obmen-barter/"]
