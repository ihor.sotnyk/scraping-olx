from fastapi import FastAPI

from routers.dopomoga_router import router as dopomoga_router
from routers.transports_router import router as transports_router
from routers.detskiy_mir_router import router as detskiy_mir_router
from routers.zapchasti_dlya_transporta_router import (
    router as zapchasti_dlya_transporta_router,
)
from routers.nedvizhimost_router import router as nedvizhimost_router
from routers.zhivotnye_router import router as zhivotnye_router
from routers.elektronika_router import router as elektronika_router
from routers.dom_i_sad_router import router as dom_i_sad_router
from routers.rabota_router import router as rabota_router
from routers.uslugi_router import router as uslugi_router
from routers.arenda_prokat_router import router as arenda_prokat_router
from routers.moda_i_stil_router import router as moda_i_stil_router
from routers.hobbi_otdyh_i_sport_router import router as hobbi_otdyh_i_sport_router
from routers.otdam_darom_router import router as otdam_darom_router
from routers.obmen_barter_router import router as obmen_barter_router


app = FastAPI()

app.include_router(dopomoga_router)
app.include_router(transports_router)
app.include_router(detskiy_mir_router)
app.include_router(zapchasti_dlya_transporta_router)
app.include_router(nedvizhimost_router)
app.include_router(zhivotnye_router)
app.include_router(elektronika_router)
app.include_router(dom_i_sad_router)
app.include_router(rabota_router)
app.include_router(uslugi_router)
app.include_router(arenda_prokat_router)
app.include_router(moda_i_stil_router)
app.include_router(hobbi_otdyh_i_sport_router)
app.include_router(otdam_darom_router)
app.include_router(obmen_barter_router)
