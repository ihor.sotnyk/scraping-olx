from fastapi import APIRouter
from olx.managers import Rabota
import json


router = APIRouter(tags=["RABOTA"], prefix="/rabota")
olx = Rabota()


@router.get("/roznichnaya-torgovlya-prodazhi-zakupki")
async def roznichnaya_torgovlya_prodazhi_zakupki():
    olx.get_roznichnaya_torgovlya_prodazhi_zakupki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/upravleniye-personalom-hr")
async def upravleniye_personalom_hr():
    olx.get_upravleniye_personalom_hr()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/obrazovanie")
async def obrazovanie():
    olx.get_obrazovanie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/banki-finansy-strakhovaniye")
async def banki_finansy_strakhovaniye():
    olx.get_banki_finansy_strakhovaniye()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/selskoye-khozyaystvo-agrobiznes-lesnoye-khozyaystvo")
async def selskoye_khozyaystvo_agrobiznes_lesnoye_khozyaystvo():
    olx.get_selskoye_khozyaystvo_agrobiznes_lesnoye_khozyaystvo()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/buhgalteriya")
async def buhgalteriya():
    olx.get_buhgalteriya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/zsu")
async def zsu():
    olx.get_zsu()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/transport-logistika")
async def transport_logistika():
    olx.get_transport_logistika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/ohrana-bezopasnost")
async def ohrana_bezopasnost():
    olx.get_ohrana_bezopasnost()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/kultura-iskusstvo")
async def kultura_iskusstvo():
    olx.get_kultura_iskusstvo()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/nedvizhimost")
async def nedvizhimost():
    olx.get_nedvizhimost()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/chastichnaya-zanyatost")
async def chastichnaya_zanyatost():
    olx.get_chastichnaya_zanyatost()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/otelno-restorannyy-biznes-turizm")
async def otelno_restorannyy_biznes_turizm():
    olx.get_otelno_restorannyy_biznes_turizm()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/stroitelstvo")
async def stroitelstvo():
    olx.get_stroitelstvo()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/domashniy-personal")
async def domashniy_personal():
    olx.get_domashniy_personal()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/meditsina-farmatsiya")
async def meditsina_farmatsiya():
    olx.get_meditsina_farmatsiya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/marketing-reklama-dizayn")
async def marketing_reklama_dizayn():
    olx.get_marketing_reklama_dizayn()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/nachalo-karery-studenty")
async def nachalo_karery_studenty():
    olx.get_nachalo_karery_studenty()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/drugie-sfery-zanyatiy")
async def drugie_sfery_zanyatiy():
    olx.get_drugie_sfery_zanyatiy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/telekommunikatsii-svyaz")
async def telekommunikatsii_svyaz():
    olx.get_telekommunikatsii_svyaz()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/krasota-fitnes-sport")
async def krasota_fitnes_sport():
    olx.get_krasota_fitnes_sport()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/it-telekom-kompyutery")
async def it_telekom_kompyutery():
    olx.get_it_telekom_kompyutery()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/proizvodstvo-energetika")
async def proizvodstvo_energetika():
    olx.get_proizvodstvo_energetika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/rabota-za-rubezhom")
async def rabota_za_rubezhom():
    olx.get_rabota_za_rubezhom()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/sto-avtomoyki")
async def sto_avtomoyki():
    olx.get_sto_avtomoyki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
