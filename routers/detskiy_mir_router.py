from fastapi import APIRouter
from olx.managers import DetskiyMir
import json


router = APIRouter(tags=["DETSKIY MIR"], prefix="/detskiy-mir")
olx = DetskiyMir()


@router.get("/detskaya-odezhda")
async def detskaya_odezhda():
    olx.get_detskaya_odezhda()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/detskaya-mebel")
async def detskaya_mebel():
    olx.get_detskaya_mebel()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tovary-dlya-shkolnikov")
async def tovary_dlya_shkolnikov():
    olx.get_tovary_dlya_shkolnikov()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/detskaya-obuv")
async def detskaya_obuv():
    olx.get_detskaya_obuv()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/igrushki")
async def igrushki():
    olx.get_igrushki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/prochie-detskie-tovary")
async def prochie_detskie_tovary():
    olx.get_prochie_detskie_tovary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/detskie-kolyaski")
async def detskie_kolyaski():
    olx.get_detskie_kolyaski()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/detskiy-transport")
async def detskiy_transport():
    olx.get_detskiy_transport()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/detskie-avtokresla")
async def detskie_avtokresla():
    olx.get_detskie_avtokresla()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/kormlenie")
async def kormlenie():
    olx.get_kormlenie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
