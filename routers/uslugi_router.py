from fastapi import APIRouter
from olx.managers import Uslugi
import json


router = APIRouter(tags=["USLUGI"], prefix="/uslugi")
olx = Uslugi()


@router.get("/avto-moto-uslugi")
async def avto_moto_uslugi():
    olx.get_avto_moto_uslugi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/klining")
async def klining():
    olx.get_klining()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/organizatsiya-prazdnikov")
async def organizatsiya_prazdnikov():
    olx.get_organizatsiya_prazdnikov()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/priem-vtorsyrya")
async def priem_vtorsyrya():
    olx.get_priem_vtorsyrya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/oborudovanie")
async def oborudovanie():
    olx.get_oborudovanie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/krasota-zdorove")
async def krasota_zdorove():
    olx.get_krasota_zdorove()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/obrazovanie")
async def obrazovanie():
    olx.get_obrazovanie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/remont-i-obsluzhivanie-tehniki")
async def remont_i_obsluzhivanie_tehniki():
    olx.get_remont_i_obsluzhivanie_tehniki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/turizm-immigratsiya")
async def turizm_immigratsiya():
    olx.get_turizm_immigratsiya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/uslugi-dlya-zhivotnyh")
async def uslugi_dlya_zhivotnyh():
    olx.get_uslugi_dlya_zhivotnyh()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/nyani-sidelki")
async def nyani_sidelki():
    olx.get_nyani_sidelki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/perevozki-arenda-transporta")
async def perevozki_arenda_transporta():
    olx.get_perevozki_arenda_transporta()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/stroitelstvo-otdelka-remont")
async def stroitelstvo_otdelka_remont():
    olx.get_stroitelstvo_otdelka_remont()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/delovye-uslugi")
async def delovye_uslugi():
    olx.get_delovye_uslugi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/ritualnye-uslugi")
async def ritualnye_uslugi():
    olx.get_ritualnye_uslugi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/bytovye-uslugi")
async def bytovye_uslugi():
    olx.get_bytovye_uslugi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/razvlechenie-foto-video")
async def razvlechenie_foto_video():
    olx.get_razvlechenie_foto_video()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/syre-materialy")
async def syre_materialy():
    olx.get_syre_materialy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/prodazha-biznesa")
async def prodazha_biznesa():
    olx.get_prodazha_biznesa()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/prochie-uslugi")
async def prochie_uslugi():
    olx.get_prochie_uslugi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
