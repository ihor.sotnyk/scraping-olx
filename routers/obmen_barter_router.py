from fastapi import APIRouter
from olx.managers import OtdamDarom
import json


router = APIRouter(tags=["OBMEN BARTER"], prefix="/obmen-barter")
olx = OtdamDarom()


@router.get("/")
async def obmen_barter():
    olx.get_obmen_barter()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
