from fastapi import APIRouter
from olx.managers import ModaIStil
import json


router = APIRouter(tags=["MODA I STIL"], prefix="/moda-i-stil")
olx = ModaIStil()


@router.get("/zhenskaya-odezhda")
async def zhenskaya_odezhda():
    olx.get_zhenskaya_odezhda()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/zhenskoe-bele-kupalniki")
async def zhenskoe_bele_kupalniki():
    olx.get_zhenskoe_bele_kupalniki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/naruchnye-chasy")
async def naruchnye_chasy():
    olx.get_naruchnye_chasy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/podarki")
async def podarki():
    olx.get_podarki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/zhenskaya-obuv")
async def zhenskaya_obuv():
    olx.get_zhenskaya_obuv()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/muzhskoe-bele-plavki")
async def muzhskoe_bele_plavki():
    olx.get_muzhskoe_bele_plavki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/aksessuary")
async def aksessuary():
    olx.get_aksessuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/spetsodezhda-i-spetsobuv")
async def spetsodezhda_i_spetsobuv():
    olx.get_spetsodezhda_i_spetsobuv()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/muzhskaya-odezhda")
async def muzhskaya_odezhda():
    olx.get_muzhskaya_odezhda()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/golovnye-ubory")
async def golovnye_ubory():
    olx.get_golovnye_ubory()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/odezhda-dlya-beremennyh")
async def odezhda_dlya_beremennyh():
    olx.get_odezhda_dlya_beremennyh()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/moda-raznoe")
async def moda_raznoe():
    olx.get_moda_raznoe()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/muzhskaya-obuv")
async def muzhskaya_obuv():
    olx.get_muzhskaya_obuv()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/dlya-svadby")
async def dlya_svadby():
    olx.get_dlya_svadby()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/krasota-zdorove")
async def krasota_zdorove():
    olx.get_krasota_zdorove()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
