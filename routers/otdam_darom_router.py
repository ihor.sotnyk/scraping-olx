from fastapi import APIRouter
from olx.managers import OtdamDarom
import json


router = APIRouter(tags=["OTDAM DAROM"], prefix="/otdam-darom")
olx = OtdamDarom()


@router.get("/")
async def otdam_darom():
    olx.get_otdam_darom()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
