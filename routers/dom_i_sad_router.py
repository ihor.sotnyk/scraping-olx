from fastapi import APIRouter
from olx.managers import DomISad
import json


router = APIRouter(tags=["DOM I SAD"], prefix="/dom-i-sad")
olx = DomISad()


@router.get("/kantstovary-rashodnye-materialy")
async def kantstovary_rashodnye_materialy():
    olx.get_kantstovary_rashodnye_materialy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/predmety-interera")
async def predmety_interera():
    olx.get_predmety_interera()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/posuda-kuhonnaya-utvar")
async def posuda_kuhonnaya_utvar():
    olx.get_posuda_kuhonnaya_utvar()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/mebel")
async def mebel():
    olx.get_mebel()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/stroitelstvo-remont")
async def stroitelstvo_remont():
    olx.get_stroitelstvo_remont()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/sadovyy-inventar")
async def sadovyy_inventar():
    olx.get_sadovyy_inventar()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/produkty-pitaniya-napitki")
async def produkty_pitaniya_napitki():
    olx.get_produkty_pitaniya_napitki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/instrumenty")
async def instrumenty():
    olx.get_instrumenty()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/hozyaystvennyy-inventar")
async def hozyaystvennyy_inventar():
    olx.get_hozyaystvennyy_inventar()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/sad-ogorod")
async def sad_ogorod():
    olx.get_sad_ogorod()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/komnatnye-rasteniya")
async def komnatnye_rasteniya():
    olx.get_komnatnye_rasteniya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/prochie-tovary-dlya-doma")
async def prochie_tovary_dlya_doma():
    olx.get_prochie_tovary_dlya_doma()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
