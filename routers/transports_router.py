from fastapi import APIRouter
from olx.managers import Transports
import json


router = APIRouter(tags=["TRANSPORTS"], prefix="/transport")
olx = Transports()


@router.get("/legkovye-avtomobili")
async def legkovye_avtomobili():
    olx.get_legkovye_avtomobili()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/spetstehnika")
async def spetstehnika():
    olx.get_spetstehnika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/pritsepy-doma-na-kolesah")
async def pritsepy_doma_na_kolesah():
    olx.get_pritsepy_doma_na_kolesah()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/gruzovye-avtomobili")
async def gruzovye_avtomobili():
    olx.get_gruzovye_avtomobili()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/selhoztehnika")
async def selhoztehnika():
    olx.get_selhoztehnika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/gruzoviki-i-spetstehnika-iz-polshi")
async def gruzoviki_i_spetstehnika_iz_polshi():
    olx.get_gruzoviki_i_spetstehnika_iz_polshi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/avtobusy")
async def avtobusy():
    olx.get_avtobusy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/vodnyy-transport")
async def vodnyy_transport():
    olx.get_vodnyy_transport()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/drugoy-transport")
async def drugoy_transport():
    olx.get_drugoy_transport()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/moto")
async def moto():
    olx.get_moto()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/avtomobili-iz-polshi")
async def avtomobili_iz_polshi():
    olx.get_avtomobili_iz_polshi()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
