from fastapi import APIRouter
from olx.managers import Nedvizhimost
import json


router = APIRouter(tags=["NEDVIZHIMOST"], prefix="/nedvizhimost")
olx = Nedvizhimost()


@router.get("/kvartiry")
async def kvartiry():
    olx.get_kvartiry()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/zemlya")
async def zemlya():
    olx.get_zemlya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/komnaty")
async def komnaty():
    olx.get_komnaty()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/kommercheskaya-nedvizhimost")
async def kommercheskaya_nedvizhimost():
    olx.get_kommercheskaya_nedvizhimost()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/doma")
async def doma():
    olx.get_doma()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/garazhy-parkovki")
async def garazhy_parkovki():
    olx.get_garazhy_parkovki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/posutochno-pochasovo")
async def posutochno_pochasovo():
    olx.get_posutochno_pochasovo()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/nedvizhimost-za-rubezhom")
async def nedvizhimost_za_rubezhom():
    olx.get_nedvizhimost_za_rubezhom()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
