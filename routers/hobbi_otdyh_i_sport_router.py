from fastapi import APIRouter
from olx.managers import HobbiOtdyhISport
import json


router = APIRouter(tags=["HOBBI"], prefix="/hobbi-otdyh-i-sport")
olx = HobbiOtdyhISport()


@router.get("/antikvariat-kollektsii")
async def antikvariat_kollektsii():
    olx.get_antikvariat_kollektsii()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/militariya")
async def militariya():
    olx.get_militariya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/bilety")
async def bilety():
    olx.get_bilety()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/muzykalnye-instrumenty")
async def muzykalnye_instrumenty():
    olx.get_muzykalnye_instrumenty()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/kvadrokoptery-i-aksessuary")
async def kvadrokoptery_i_aksessuary():
    olx.get_kvadrokoptery_i_aksessuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/poisk-poputchikov")
async def poisk_poputchikov():
    olx.get_poisk_poputchikov()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/sport-otdyh")
async def sport_otdyh():
    olx.get_sport_otdyh()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/knigi-zhurnaly")
async def knigi_zhurnaly():
    olx.get_knigi_zhurnaly()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/poisk-grupp-muzykantov")
async def poisk_grupp_muzykantov():
    olx.get_poisk_grupp_muzykantov()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/velo")
async def velo():
    olx.get_velo()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/cd-dvd-plastinki")
async def cd_dvd_plastinki():
    olx.get_cd_dvd_plastinki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/drugoe")
async def drugoe():
    olx.get_drugoe()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
