from fastapi import APIRouter
from olx.managers import Elektronika
import json


router = APIRouter(tags=["ELEKTRONIKA"], prefix="/elektronika")
olx = Elektronika()


@router.get("/telefony-i-aksesuary")
async def telefony_i_aksesuary():
    olx.get_telefony_i_aksesuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/audiotehnika")
async def audiotehnika():
    olx.get_audiotehnika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tehnika-dlya-doma")
async def tehnika_dlya_doma():
    olx.get_tehnika_dlya_doma()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/aksessuary-i-komplektuyuschie")
async def aksessuary_i_komplektuyuschie():
    olx.get_aksessuary_i_komplektuyuschie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/kompyutery-i-komplektuyuschie")
async def kompyutery_i_komplektuyuschie():
    olx.get_kompyutery_i_komplektuyuschie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/igry-i-igrovye-pristavki")
async def igry_i_igrovye_pristavki():
    olx.get_igry_i_igrovye_pristavki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tehnika-dlya-kuhni")
async def tehnika_dlya_kuhni():
    olx.get_tehnika_dlya_kuhni()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/prochaja-electronika")
async def prochaja_electronika():
    olx.get_prochaja_electronika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/foto-video")
async def foto_video():
    olx.get_foto_video()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/planshety-el-knigi-i-aksessuary")
async def planshety_el_knigi_i_aksessuary():
    olx.get_planshety_el_knigi_i_aksessuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/klimaticheskoe-oborudovanie")
async def klimaticheskoe_oborudovanie():
    olx.get_klimaticheskoe_oborudovanie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/remont-i-obsluzhivanie-tehniki")
async def remont_i_obsluzhivanie_tehniki():
    olx.get_remont_i_obsluzhivanie_tehniki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tv-videotehnika")
async def tv_videotehnika():
    olx.get_tv_videotehnika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/noutbuki-i-aksesuary")
async def noutbuki_i_aksesuary():
    olx.get_noutbuki_i_aksesuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/individualnyy-uhod")
async def individualnyy_uhod():
    olx.get_individualnyy_uhod()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
