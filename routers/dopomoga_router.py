from fastapi import APIRouter
from olx.managers import Dopomoga
import json


router = APIRouter(tags=["DOPOMOGA"], prefix="/dopomoga")
olx = Dopomoga()


@router.get("/potribna_dopomoha")
async def potribna_dopomoha():
    olx.get_potribna_dopomoha()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/proponuiu_dopomohu")
async def proponuiu_dopomohu():
    olx.get_proponuiu_dopomohu()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
