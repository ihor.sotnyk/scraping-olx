from fastapi import APIRouter
from olx.managers import ZapchastiDlyaTransporta
import json


router = APIRouter(
    tags=["ZAPCHASTI DLYA TRNSPORTA"], prefix="/zapchasti-dlya-transporta"
)
olx = ZapchastiDlyaTransporta()


@router.get("/avtozapchasti")
async def avtozapchasti():
    olx.get_avtozapchasti()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/gps-navigatory-videoregistratory")
async def gps_navigatory_videoregistratory():
    olx.get_gps_navigatory_videoregistratory()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/motoaksessuary")
async def motoaksessuary():
    olx.get_motoaksessuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/aksessuary-dlya-avto")
async def aksessuary_dlya_avto():
    olx.get_aksessuary_dlya_avto()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/transport-na-zapchasti-avtorazborki")
async def transport_na_zapchasti_avtorazborki():
    olx.get_transport_na_zapchasti_avtorazborki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/masla-i-avtokhimiya")
async def masla_i_avtokhimiya():
    olx.get_masla_i_avtokhimiya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/avtozvuk-i-multimedia")
async def avtozvuk_i_multimedia():
    olx.get_avtozvuk_i_multimedia()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/motozapchasti")
async def motozapchasti():
    olx.get_motozapchasti()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/prochie-zapchasti")
async def prochie_zapchasti():
    olx.get_prochie_zapchasti()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/shiny-diski-i-kolesa")
async def shiny_diski_i_kolesa():
    olx.get_shiny_diski_i_kolesa()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/motoekipirovka")
async def motoekipirovka():
    olx.get_motoekipirovka()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
