from fastapi import APIRouter
from olx.managers import Zhivotnye
import json


router = APIRouter(tags=["ZHIVOTNYE"], prefix="/zhivotnye")
olx = Zhivotnye()


@router.get("/besplatno-zhivotnye-i-vyazka")
async def besplatno_zhivotnye_i_vyazka():
    olx.get_besplatno_zhivotnye_i_vyazka()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/besplatno-zhivotnye-i-vyazka")
async def ptitsy():
    olx.get_ptitsy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/drugie-zhivotnye")
async def drugie_zhivotnye():
    olx.get_drugie_zhivotnye()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/sobaki")
async def sobaki():
    olx.get_sobaki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/gryzuny")
async def gryzuny():
    olx.get_gryzuny()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tovary-dlya-zhivotnyh")
async def tovary_dlya_zhivotnyh():
    olx.get_tovary_dlya_zhivotnyh()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/koshki")
async def koshki():
    olx.get_koshki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/reptilii")
async def reptilii():
    olx.get_reptilii()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/vyazka")
async def vyazka():
    olx.get_vyazka()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/akvariumnye-rybki")
async def akvariumnye_rybki():
    olx.get_akvariumnye_rybki()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/selskohozyaystvennye-zhivotnye")
async def selskohozyaystvennye_zhivotnye():
    olx.get_selskohozyaystvennye_zhivotnye()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/byuro-nahodok")
async def byuro_nahodok():
    olx.get_byuro_nahodok()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
