from fastapi import APIRouter
from olx.managers import ArendaProkat
import json


router = APIRouter(tags=["ARENDA PROKAT"], prefix="/arenda-prokat")
olx = ArendaProkat()


@router.get("/transport-spetstehnika")
async def transport_spetstehnika():
    olx.get_transport_spetstehnika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tovary-med-naznacheniya")
async def tovary_med_naznacheniya():
    olx.get_tovary_med_naznacheniya()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/odezhda-aksessuary")
async def odezhda_aksessuary():
    olx.get_odezhda_aksessuary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/velosipedy-moto")
async def velosipedy_moto():
    olx.get_velosipedy_moto()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tehnika-elektronika")
async def tehnika_elektronika():
    olx.get_tehnika_elektronika()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/detskaya-odezhda-tovary")
async def detskaya_odezhda_tovary():
    olx.get_detskaya_odezhda_tovary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/oborudovanie")
async def oborudovanie():
    olx.get_oborudovanie()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/tovary-dlya-meropriyatiy")
async def tovary_dlya_meropriyatiy():
    olx.get_tovary_dlya_meropriyatiy()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/drugoe")
async def drugoe():
    olx.get_drugoe()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/instrumenty")
async def instrumenty():
    olx.get_instrumenty()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


@router.get("/sport-turisticheskie-tovary")
async def sport_turisticheskie_tovary():
    olx.get_sport_turisticheskie_tovary()
    with open(f"olx/data/{olx.file_name}.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        return data
